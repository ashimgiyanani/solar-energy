# -*- coding: utf-8 -*-
"""
@file: %(file)
@description: 
@syntax:
@inputs:

@outputs:
@example:

See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

@Author: %(username), PhD candidate, TU Delft
Wind energy department, TU Delft
email address: ashimgiyanani@yahoo.com
March 2018; Last revision: 06-04-2018
Created on %(date)s
@version: 0.1
"""

import sys
import math
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time
import copy

import glob
import pathlib
import csv
# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Eingabe parameter
# peak power in kWp
Pp = 15 
# multiplication factor fuer Verbrauch lasten
lastFactor=2 # , best combi  =2
#pvFactor = np.array([5/15,10/15,1])  # multiplication factor fuer PV leistung, best combi =0.7
pvFactor = np.array([10/15])
PV_kwp = pvFactor*Pp
Sp0 = np.arange(0,110,10) # Speicher kapazitaet Eingabe in kWh
#Sp0 = np.array([5])
freq = 0.25 # Frequenz in Stunden, 1 Stunde = 1, 15 min = 1/4 Stunden
scale = 1.0 # scale factor for 
Nd = 365 # no. of days, '1' day for time series related, '3' day for speicher funcktionsweise und '365' fuer Eigenverbrauch und Autarkiesgrad zu rechnen
preis_N_esp = 0.11 # 11c/kWh
preis_N_ent = 0.29 # 29c/kWh
kosten_PV = 1725*PV_kwp + 100*PV_kwp + 150*PV_kwp
Lebensdauer_Anlage = 20 # no.of years
Lebensdauer_batt = 4 # no. of years
Nbatt = Lebensdauer_Anlage/Lebensdauer_batt # Anzahl Batterien
kosten_Speicher = 3000*Sp0*(Nbatt-1)
kosten_Fix = 5000
Batterie = 1 # '1' fuer Batterie basierte rechnungen, '0' - fuer ohne batterie installation
plotFig = 0

# read load profile
file1 = r"C:\Users\Ilhan\Desktop\load_profile1.csv"
data1 = pd.read_csv(file1, sep=',',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=4)

# read annual PV profile
file2 = r"C:\Users\Ilhan\Desktop\PV_profile_edited.csv"
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0)

tf = pd.to_datetime(np.arange('2013-01-01', '2014-01-01', np.timedelta64(1,'D'), dtype='datetime64'))

# Taegliche mittelwerte
Td = np.linspace(1,365,365) # 1-day timestamp
Pel_1d  = np.zeros(Td.size)
for i in range(0,Td.size):
    ri = i*24
    Pel_1d[i] = np.nansum(data2.Pel_W[np.arange(ri, ri+24)])

# Jahres profil mit Stundliche daten
Pel_h = pvFactor*Pel_1d/1000 # Pel in kW

# indicien fuer die Saisonen
tw1a = int(np.ravel(np.where(tf=='01-01-2013')))
tw2a = int(np.ravel(np.where(tf=='20-03-2013')))
tu1a = int(np.ravel(np.where(tf=='21-03-2013')))
tu2a = int(np.ravel(np.where(tf=='14-05-2013')))
ts1 = int(np.ravel(np.where(tf=='15-05-2013')))
ts2 = int(np.ravel(np.where(tf=='14-09-2013')))
tu1b = int(np.ravel(np.where(tf=='15-09-2013')))
tu2b = int(np.ravel(np.where(tf=='31-10-2013')))
tw1b = int(np.ravel(np.where(tf=='11-01-2013')))
tw2b = int(np.ravel(np.where(tf=='31-12-2013')))

Pel_wb = Pel_h[tw1b:tw2b]
Pel_wa = Pel_h[tw1a:tw2a]
Pel_ua = Pel_h[tu1a:tu2a]
Pel_ub = Pel_h[tu1b:tu2b]

Pel_s = Pel_h[ts1:ts2]
Pel_w = np.concatenate((Pel_wb,Pel_wa))
Pel_u = np.concatenate((Pel_ua,Pel_ub))

# PV Ertraege
plt.figure()
plt.plot(Td,Pel_h[:]) # eingabe hier
plt.xlabel('Tage')
plt.ylabel('PV erzeugte energie (kW)')
plt.title('Datenprofil der Photovoltaikanlage - Jahresansicht')
plt.show()
if (plotFig==1):
    plt.savefig('Jahresansicht_PV_TaeglicheWerte.png')

# PV Ertraege Saison
tt1 = np.linspace(1,Pel_ub.size,Pel_ub.size)
plt.figure()
plt.plot(tt1,Pel_ub) # eingabe hier
plt.xlabel('Zeit (Tagen)')
plt.ylabel('PV erzeugte energie (kW)')
plt.title('Datenprofil der Photovoltaikanlage - Herbst')
plt.show()
#if (plotFig==1):
plt.savefig('Herbst_PV_Tag.png')

# Verbrauch von 15min Werte zu Stuendliche Werte rechnen
# loop durch Winter daten
samW = lastFactor*np.nansum(data1.sam_w)/1000 # in kW
sonW = lastFactor*np.nansum(data1.son_w)/1000 # in kW
werkW = lastFactor*np.nansum(data1.werk_w)/1000 # in kW
# loop durch Somer daten
samS = lastFactor*np.nansum(data1.sam_s)/1000 # in kW
sonS = lastFactor*np.nansum(data1.son_s)/1000 # in kW
werkS = lastFactor*np.nansum(data1.werk_s)/1000 # in kW
# loop durch Uebergangszeit daten
samU = lastFactor*np.nansum(data1.sam_u)/1000 # in kW
sonU = lastFactor*np.nansum(data1.son_u)/1000 # in kW
werkU = lastFactor*np.nansum(data1.werk_u)/1000 # in kW

werkW_mat = np.tile(werkW,(5,1))
werkS_mat = np.tile(werkS,(5,1))
werkU_mat = np.tile(werkU,(5,1))
week_w = np.vstack((werkW_mat,samW,sonW))
week_s = np.vstack((werkS_mat,samS,sonS))
week_u = np.vstack((werkU_mat,samU,sonU))

Ver_wa = np.tile(week_w,(int(np.floor((tw2a-tw1a)/7)),1)) # Verbrauch winter Jan-Mar
Ver_ua = np.tile(week_u,(int(np.ceil((tu2a-tu1a)/7)),1)) # Verbrauch Fruehling
Ver_s = np.tile(week_s,(int(np.floor((ts2-ts1)/7)),1)) # Verbrauch Sommer
Ver_ub = np.tile(week_u,(int(np.ceil((tu2b-tu1b)/7)),1)) # Verbrauch Herbst
Ver_wb = np.tile(week_w,(int(np.ceil((tw2b-tw1b)/7)),1)) # Verbrauch winter Nov-Dez

#
Ver_h = np.vstack((Ver_wa,Ver_ua,Ver_s,Ver_ub,Ver_wb,samW)).flatten() # Verbrauch ganze Jahr note the adjustment at the end of the year
Ver_w = np.vstack((Ver_wb,samW,Ver_wa)).flatten() # Verbrauch Winter adjustment as the no. of days didn't match 365 days of the year
Ver_u = np.vstack((Ver_ua,Ver_ub)).flatten() # Verbrauch Ubergangszeit
Ver_s = Ver_s.flatten() # Verbrauch Sommer

# Verbrauchs Last Statistik
plt.figure()
plt.plot(Td,Ver_h[:]) # eingabe 
plt.xlabel('Zeit (h)')
plt.ylabel('Standardlast (kW)')
plt.title('Datenprofil der Verbrauch (last) - Jahresansicht')
plt.show()
#if (plotFig==1):
plt.savefig('Jahresansicht_Last_Tag.png')

# Verbrauch Last Statistik Saison
tt1 = np.linspace(1,Ver_ub.size,Ver_ub.size)
plt.figure()
plt.plot(tt1,Ver_ub)
plt.xlabel('Zeit (Tagen)')
plt.ylabel('Standardlast (kW)')
plt.title('Datenprofil der Verbrauch (last) - Herbst')
plt.show()
#if (plotFig==1):
plt.savefig('Herbst_Last_Tag.png')
