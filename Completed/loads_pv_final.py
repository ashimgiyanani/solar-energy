# -*- coding: utf-8 -*-
"""
@file: %(file)
@description: 
@syntax:
@inputs:

@outputs:
@example:

See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

@Author: %(username), PhD candidate, TU Delft
Wind energy department, TU Delft
email address: ashimgiyanani@yahoo.com
March 2018; Last revision: 06-04-2018
Created on %(date)s
@version: 0.1
"""

import sys
import math
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time
import copy

import pathlib
import csv
# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Eingabe parameter
# peak power in kWp
Pp = 15 
# multiplication factor fuer Verbrauch lasten
lastFactor=2 # , best combi  =2
pvFactor = np.array([5/15,10/15,1])  # multiplication factor fuer PV leistung, best combi =0.7
#pvFactor = np.array([0.7])
PV_kwp = pvFactor*Pp
Sp0_1 = np.arange(0,1,0.1) # Speicher kapazitaet Eingabe in kWh
Sp0_2 = np.arange(1,10,1)
Sp0_3 = np.arange(10,60, 10)
Sp0 = np.concatenate([Sp0_1, Sp0_2, Sp0_3], axis=0)
#Sp0 = np.array([5])
freq = 0.25 # Frequenz in Stunden, 1 Stunde = 1, 15 min = 1/4 Stunden
scale = 1.0 # scale factor for 
Nd = 365 # no. of days, '1' day for time series related, '3' day for speicher funcktionsweise und '365' fuer Eigenverbrauch und Autarkiesgrad zu rechnen
preis_N_esp = 0.11 # 11c/kWh
preis_N_ent = 0.29 # 29c/kWh
kosten_PV = 1265*PV_kwp
Lebensdauer_Anlage = 20 # no.of years
Lebensdauer_batt = 15 # no. of years
Nbatt = Lebensdauer_Anlage/Lebensdauer_batt # Anzahl Batterien
kosten_Speicher = 800*Sp0*(Nbatt-1)
kosten_Fix = 150
Batterie = 1 # '1' fuer Batterie basierte rechnungen, '0' - fuer ohne batterie installation
plotFig = 0 # 0 - Grafik Bilder nicht speichern,  '1' - speichern

# read load profile
file1 = r"C:\Users\Ilhan\Desktop\load_profile1.csv"
data1 = pd.read_csv(file1, sep=',',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=4)

# read annual PV profile
file2 = r"C:\Users\Ilhan\Desktop\PV_profile_edited.csv"
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0)

tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(15,'m'), dtype='datetime64'))

# Zeit vorstellungen
Nhr = int(Nd*24/freq) # number of hours/15min in Nd
tt = np.linspace(1,Nhr,Nhr) # Zeitreihe je nach der Eingabe Nd
yr = np.linspace(1,365,365*24/freq) # Jahr
dd = np.linspace(1,24,24/freq) # tag
wk = np.linspace(1,7,7*24/freq) # Woche
mon = np.linspace(1,30,30*24/freq) # Monat

# interpolation um die Pel_W datei in 15 min zu rechnen
Th = np.linspace(1,365,365*24) # hourly timestamp
T15m = np.linspace(1,365,365*24/freq) # 15 minute timestamp
Pel_15m = np.interp(T15m,Th,data2.Pel_W[0:365*24]) # 15 minute Pel

# Taegliche mittelwerte
Td = np.linspace(1,365,365) # 1-day timestamp
Pel_1d  = np.zeros(Td.size)
for i in range(0,Td.size):
    ri = i*24
    Pel_1d[i] = np.nansum(data2.Pel_W[np.arange(ri, ri+24)])
    
data_Ag = np.zeros([Sp0.size,pvFactor.size])
data_evQ = np.zeros([Sp0.size,pvFactor.size])
data_N_esp = np.zeros([Sp0.size,pvFactor.size])
data_N_ent = np.zeros([Sp0.size,pvFactor.size])
data_Ncyc = np.zeros([Sp0.size,pvFactor.size])
data_kosten = np.zeros([Sp0.size,pvFactor.size])

for iv in range(0,pvFactor.size):
    # Jahres profil mit Stundliche daten
    Pel_h = pvFactor[iv]*Pel_15m/1000 # Pel in kW
    
    # Last profil fuer den gesamten Jahr
    # Verbrauch stuendliche Mittelwerte in einem Tag
    samW = np.zeros([int(24/freq),1]) # Samstag Winter
    sonW = np.zeros([int(24/freq),1]) # Sonntag Winter
    werkW = np.zeros([int(24/freq),1]) # Werktag Winter
    # Tages Eigenvarbrauch in Sommer
    samS = np.zeros([int(24/freq),1]) # Samstag Sommer
    sonS = np.zeros([int(24/freq),1]) # Sonntag Sommer
    werkS = np.zeros([int(24/freq),1]) # Werktag Sommer
    # Tages Eigenvarbrauch in Uebergangszeit
    samU = np.zeros([int(24/freq),1]) # Samstag Uebergangszeit
    sonU = np.zeros([int(24/freq),1]) # Sonntag Uebergangszeit
    werkU = np.zeros([int(24/freq),1]) # Werktag Uebergangszeit
    
    Ver_h3d = np.zeros([int(24/freq)*3,1])
    
    samW = lastFactor*data1.sam_w/1000
    sonW = lastFactor*data1.son_w/1000
    werkW = lastFactor*data1.werk_w/1000
    samS = lastFactor*data1.sam_s/1000
    sonS = lastFactor*data1.son_s/1000
    werkS = lastFactor*data1.werk_s/1000
    samU = lastFactor*data1.sam_u/1000
    sonU = lastFactor*data1.son_u/1000
    werkU = lastFactor*data1.werk_u/1000
    
    # indicien fuer die Saisonen
    tw1a = int(np.ravel(np.where(tf=='01-01-2013')))
    tw2a = int(np.ravel(np.where(tf=='20-03-2013 23:45:00')))
    tu1a = int(np.ravel(np.where(tf=='21-03-2013')))
    tu2a = int(np.ravel(np.where(tf=='14-05-2013 23:45:00')))
    ts1 = int(np.ravel(np.where(tf=='15-05-2013')))
    ts2 = int(np.ravel(np.where(tf=='14-09-2013 23:45:00')))
    tu1b = int(np.ravel(np.where(tf=='15-09-2013')))
    tu2b = int(np.ravel(np.where(tf=='31-10-2013 23:45:00')))
    tw1b = int(np.ravel(np.where(tf=='11-01-2013')))
    tw2b = int(np.ravel(np.where(tf=='31-12-2013 23:45:00')))
    
    werkW_mat = np.tile(werkW,(5,1))
    werkS_mat = np.tile(werkS,(5,1))
    werkU_mat = np.tile(werkU,(5,1))
    week_w = np.vstack((werkW_mat,samW,sonW))
    week_s = np.vstack((werkS_mat,samS,sonS))
    week_u = np.vstack((werkU_mat,samU,sonU))
    
    Ver_wa = np.tile(week_w,(int(np.floor((tw2a-tw1a)/(168/freq))),1))
    Ver_ua = np.tile(week_u,(int(np.ceil((tu2a-tu1a)/(168/freq))),1))
    Ver_s = np.tile(week_s,(int(np.floor((ts2-ts1)/(168/freq))),1))
    Ver_ub = np.tile(week_u,(int(np.ceil((tu2b-tu1b)/(168/freq))),1))
    Ver_wb = np.tile(week_w,(int(np.ceil((tw2b-tw1b)/(168/freq))),1))
    
    Ver_h = np.vstack((Ver_wa,Ver_ua,Ver_s,Ver_ub,Ver_wb,samW)).flatten()
    Ver_w = np.vstack((Ver_wb,samW,Ver_wa)).flatten()
    Ver_u = np.vstack((Ver_ua,Ver_ub)).flatten()
    Ver_s = Ver_s.flatten()
    
    Pel_wb = Pel_h[tw1b:tw2b]
    Pel_wa = Pel_h[tw1a:tw2a]
    Pel_ua = Pel_h[tu1a:tu2a]
    Pel_ub = Pel_h[tu1b:tu2b]
    
    Pel_s = Pel_h[ts1:ts2]
    Pel_w = np.concatenate((Pel_wb,Pel_wa))
    Pel_u = np.concatenate((Pel_ua,Pel_ub))
    
    # PV Ertraege 
    plt.figure()
    plt.plot(T15m, Pel_h[:]) # eingabe hier
    plt.xlabel('Tage')
    plt.ylabel('PV erzeugte energie (kWh)')
    plt.title('Datenprofil der Photovoltaikanlage - Jahresansicht')
    plt.show()
    if (plotFig==1):
        plt.savefig('Jahresansicht_PV_TaeglicheWerte.png')
    
    # PV Ertraege Saison #eingabe für Pel_s; Pel_w; Pel_u (ua=Frühling)(ub=Herbst)
    tt1 = np.linspace(1,Pel_s.size/(24/freq),Pel_w.size)
    plt.figure()
    plt.plot(tt1,Pel_w) # eingabe hier
    plt.xlabel('Zeit (Tagen)')
    plt.ylabel('PV erzeugte energie (kWh)')
    plt.title('Datenprofil der Photovoltaikanlage - Winter')
    plt.show()
    if (plotFig==1):
        plt.savefig('Winter_PV.png')
    
    
    # Verbrauchs Last Statistik
    plt.figure()
    plt.plot(tt,Ver_h[0:Nhr]) # eingabe 
    plt.xlabel('Zeit (h)')
    plt.ylabel('Standardlast (kWh)')
    plt.title('Datenprofil der Verbrauch (last) - Jahresansicht')
    plt.show()
    if (plotFig==1):
        plt.savefig('Jahresansicht_Last.png')
    
    # Verbrauch Last Statistik Saison #eingabe für Ver_w; Ver_u; Ver_s; (ua=Frühling ub=Herbst)
    tt1 = np.linspace(1,Ver_s.size/(24/freq),Ver_s.size)
    plt.figure()
    plt.plot(tt1,Ver_s)
    plt.xlabel('Zeit (Tagen)')
    plt.ylabel('Standardlast (kWh)')
    plt.title('Datenprofil der Verbrauch (last) -Sommer')
    plt.show()
    if (plotFig==1):
        plt.savefig('Sommer_Last.png')
    
    #-----------------------------------------------------------------------------------------------------
    
    # Verbrauch mit PV energie    
    Pel_hd = scale*Pel_h[0:0+Nd*int(24/freq)] # 1 Tag im Winter-Januar
    hd = np.arange(1,Nd*(24/freq)+1)
    if (Nd ==3):
        Ver_hd = np.concatenate([samW,sonW,werkW], axis=0)
    elif (Nd==1):
        Ver_hd = samW
    elif (Nd==365):
        Ver_hd = Ver_h
    else:
        print("Nd is negative")
        
    # Wir nehmen an dass der Speicher eine Anfangs Zustand von 5000 W 
    #Sp0 = np.array([5])
    # Unterschied zwichen PV ertraege und Verbrauch
    dP = (np.asarray(Pel_hd[:]) - np.ravel(Ver_hd)) # dP/dt slope of the curve
    # Unterschied samt Speicher kapazitaet
    nr = dP.size
    nc = Sp0.size
    Sp = np.zeros([nr,nc])
    Ncyc = np.zeros(nc)
    for j in range(0,Sp0.size):
        count = int(0)
        S = np.zeros(nr)
        S[0] = Sp0[j]    
        for i in range(1,dP.size):
            if (dP[i] < 0) & (S[i-1] <= 0): # Netz Bezug
                S[i] = 0
            elif (dP[i] < 0) & (S[i-1] > 0): # 
                S[i] = S[i-1] + dP[i]
                if (S[i-1]==S[0]) & (i!=1):
                    count = count+1
            elif ((dP[i] > 0) & (S[i-1] <= 0)):
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] > 0) & (S[i-1] < S[0]):
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] >= S[0]):
                S[i] = S[0]
            else:
                print('something went wrong at i=',int(i))
        S[S<0] = 0
        S[S>S[0]] = S[0]
        Sp[:,j] = copy.copy(S)
        Ncyc[j] = count
    
    #-------Work on this
    # Batteria behaviour at full and no load condition
    N_esp = np.zeros([nr, nc])
    N_ent = np.zeros([nr, nc])
    Splus = np.zeros([nr, nc])
    Sminus = np.zeros([nr,nc])
    for ik in range(0,Sp0.size):
            k_ul = np.ravel(np.where(Sp[:,ik]==Sp0[ik])) # speicher voll index
            k_ll = np.ravel(np.where(Sp[:,ik] == 0)) # speicher leer index
            k_plus = np.ravel(np.where((dP>0) & (Sp[:,ik] < Sp0[ik]))) # speicher ladung index
            k_minus = np.ravel(np.where((dP<0) & (Sp[:,ik]> 0))) # speicher entladung index

            k_esp = np.ravel(np.where((Sp[:,ik]>Sp0[ik]) & (dP>=0))) # netz einspeisen index
            k_ent = np.ravel(np.where((Sp[:,ik]<=0) & (dP<0))) # netz entnehmen index

            # Speicher ladung und entladung
            Splus[k_plus, ik] = dP[k_plus] # speicher ladung werte
            Sminus[k_minus, ik] = dP[k_minus] # speicher entladung werte
            # Netz einspeisen und entnehmen
            N_esp[k_ul,ik] = dP[k_ul] # energie ins netz eingespeist
            N_ent[k_ll,ik] = dP[k_ll] # energie von netz entnommen
          
            
    plt.figure()
    plt.plot(hd, Ver_hd,'r-',label="Verbrauch")
    plt.plot(hd, Pel_hd, 'b-',label="PV")
    plt.plot(np.arange(0,Nd*(24/freq)), Sp[:,3], 'g-',label="Speicher")
    #plt.plot(np.arange(0,Nd*(24/freq)),Sp[1:],'r.', label='og')
    #plt.plot(hd,N_esp,'b.',label='N_esp')
    #plt.plot(hd,N_ent,'g.', label='N_ent')
    plt.legend(loc=1)
    plt.xlabel('Zeit (h)')
    plt.ylabel('Energie (kW)')
    plt.title('Energie Stuendliche Mittelung')
    ax = plt.gca()
    xmin, xmax = ax.get_xlim()
    custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
    ax.set_xticks(custom_ticks)
    ax.set_xticklabels(np.round(custom_ticks/(4*24)))
    if (plotFig==1):
        plt.savefig('Speicher_funktionsweise_3Tage.png')
    
    #------------------------------------------------------------------------------------------------------------------
    ev_J = np.zeros([nc])
    evQ = np.zeros([nc])
    Ag = np.zeros([nc])
    # Produktion im Jahr
    Pel_J = sum(Pel_h)
    
    if Batterie==1:
        # Eigen verbrauch = (Direcktverbrauch + Batterieentladung) /(Direcktverbrauch + Batterieladung + Netzeinspeisung)
        # Netzeinspeisung ist eine wichtige optimierungsgrosse
        for ik in range(0,Sp0.size):
            evQ[ik] = (sum(Ver_h) + abs(sum(Sminus[:,ik])))/(sum(Ver_h) + sum(Splus[:,ik]) + sum(N_esp[1:,ik]))*100
            # Autarkiegrad = (Direcktverbrauch + Batterieentladung) /(Direcktverbrauch + Batterieentladung + Netzbezug)
            Ag[ik] = (sum(Ver_h) + abs(sum(Sminus[:,ik])))/(sum(Ver_h) + abs(sum(Sminus[:,ik])) + abs(sum(N_ent[1:,ik])))*100
    else:
        # Eigenverbrauch rechnen =  Erzeugten Energie - eingespeiste Energie
        # indicien finden wo energie erzeugt wird
        ip  = [i for (i,val) in enumerate(Pel_h) if val!=0]
        # minimum finden 
        for ik in range(0,Sp0.size):
            ev_J[ik] = sum(np.fmin(Pel_h[ip], np.ravel(Ver_h[ip]))) 
            # eigenverbrauchsquote im jahr
            evQ[ik] = (ev_J[ik]/Pel_J)*100
            Ag[ik] = (ev_J[ik]/sum(Ver_h))*100
            
    data_evQ[:,iv] = copy.copy(evQ)
    data_Ag[:,iv] = copy.copy(Ag)
    data_N_esp[:,iv] = copy.copy(np.sum(N_esp,axis=0))
    data_N_ent[:,iv] = copy.copy(np.sum(N_ent,axis=0))
    data_Ncyc[:,iv] = copy.copy(Ncyc)
#    data_kosten[:,iv] = kosten_Speicher/(Pel_J-sum(Ver_h)) + kosten_PV[iv]/Pel_J
    data_kosten[:,iv] = kosten_Speicher + kosten_PV[iv]

    
# Gesamtkosten = kosten_Speicher/(PV - Ver) + kosten_PV/Pel_J + kosten_Fix/Pel_J
gesamtKosten = data_kosten + kosten_Fix + abs(data_N_ent)*preis_N_ent/100
# Gesamterträge €/ kWh = Einspeisevergütung + Stromkostenersparnis
gesamtErtraege = abs(data_N_esp)*preis_N_esp/100 + sum(Ver_h)*preis_N_ent/100
# Erloese
Erloese = abs(data_N_esp)*preis_N_esp/100
# Kosten
Kosten = (np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))/Lebensdauer_Anlage + abs(data_N_ent)*preis_N_ent/100
# Gewinn = Erloese - gesamtKosten
Gewinn = Erloese - Kosten
# Amortisation
#Amortisation = gesamtKosten/(gesamtErtraege - kosten_Fix/Lebensdauer_Anlage)
Amortisation = (gesamtKosten/(gesamtErtraege - kosten_Fix))/Lebensdauer_Anlage

# eigenverbrauchsquote vs Batterigrosse
plt.figure()
plt.semilogx(Sp0,data_evQ)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Eigenverbrauchsquote (%)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Eigenverbrauchsquote_BatterieGr.png')

# Autarkiegrad vs Batteriegrosse
plt.figure()
plt.semilogx(Sp0,data_Ag)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Autarkiegrad (%)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Autarkiegrad_BatterieGr.png')

# Energie aus dem Netz
plt.figure()
plt.semilogx(Sp0,abs(data_N_ent))
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Netz Bezug (kWh/a)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('NetzBezug_BatterieGr.png')

# Einspeisestrom
plt.figure()
plt.semilogx(Sp0,data_N_esp)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Netz Einspeisen (kWh/a)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('NetzEinspeisen_BatterieGr.png')

# Ladezyklen
plt.figure()
plt.semilogx(Sp0,data_Ncyc)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Ladezyklen (-)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Ladezyklen.png')

# Kosten fuer Energie aus dem Netz
plt.figure()
plt.semilogx(Sp0,abs(data_N_ent)*preis_N_ent/100)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Netz Bezug kosten (€)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('NetzBezug_preis.png')

# Einspeiseverguetung fuer Energie ins Netz
plt.figure()
plt.semilogx(Sp0,data_N_esp*preis_N_esp/100)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Netz Einspeisevergütung (€)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('NetzEinspeisen_preis.png')

# kosten Gesamtkurve
plt.figure()
plt.semilogx(Sp0,gesamtKosten)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Gesamt kosten (Eur/kWh/a)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Kosten_gesamtkurve.png')

# Ertraege Gesamtkurve
plt.figure()
plt.semilogx(Sp0,gesamtErtraege)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Gesamt Ertraege (Eur/kWh/a)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Ertraege_gesamtkurve.png')

# Amortisation
plt.figure()
plt.semilogx(Sp0, Amortisation)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Amortisation Jahre (Jahre)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Amortisation.png')

# Gewinn
plt.figure()
plt.semilogx(Sp0, Gewinn)
plt.grid(True)
plt.xlabel('Batteriegrosse (kWh)')
plt.ylabel('Gewinn (Eur)')
plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
plt.savefig('Gewinn.png')


#----------------------------------------------------------------------------------
# Nicht mehr benutzte Zeilen
#----------------------------------------------------------------------------------

## Jahres profil mit Taegliche Mittelung
#Pel_d = np.zeros([365,1])
#for id in range(0,365):
#    ri = id*24
#    Pel_d[id] = np.nansum(Pel_h[np.arange(ri,ri+23)])
##    time_d[id] = pd.to_datetime(data2.time[ri])
#
#
#plt.figure()
#plt.plot(Pel_d,'.')
#xlabel('Zeit (Tagen)')
#ylabel('PV power (kW)')
#title('Jahresansicht Taegliche Mittelwerte')
#plt.show()
#del(ri,id)
#
## Jahres profil mit Wochentlichen Mittelung
#Pel_w = np.zeros([52,1])
#for id in range(0,51):
#    ri = id*24*7
#    Pel_w[id] = np.nansum(Pel_h[np.arange(ri,ri+168)])
#    
#    
#plt.figure()
#plt.plot(Pel_w,'.')
#xlabel('Zeit (Wochen)')
#ylabel('PV power (kW)')
#title('Jahresansicht Wochentliche Mittelwerte')
#plt.show()
#
## Verbrauch Statistik samstag
#plt.figure()
#plt.plot(data1.time,data1.sam_w/1000,'-')
#xlabel('Zeit (15min)')
#ylabel('Verbrauch (kW)')
#title('Verbrauch Winter-Samstag (15min Mittelwerte)')
#ax = plt.gca()
#xmin, xmax = ax.get_xlim()
#custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
#ax.set_xticks(custom_ticks)
#ax.set_xticklabels(ceil(custom_ticks*24/100))
#
## Verbrauch Statistik Sonntag
#plt.figure()
#plt.plot(data1.time,data1.son_w/1000,'-')
#xlabel('Zeit (15min)')
#ylabel('Verbrauch (kW)')
#title('Verbrauch Winter-Sonntag (15min Mittelwerte)')
#ax = plt.gca()
#ax.set_xticks(custom_ticks)
#ax.set_xticklabels(ceil(custom_ticks*24/100))
#
#
## Verbrauch Statistik Werktag 
#plt.figure()
#plt.plot(data1.time,data1.werk_w/1000,'-')
#xlabel('Zeit (15min)')
#ylabel('Verbrauch werktag (kW)')
#title('Verbrauch Winter-werktag (15min Mittelwerte)')
#ax = plt.gca()
#ax.set_xticks(custom_ticks)
#ax.set_xticklabels(ceil(custom_ticks*24/100))

# Verbrauch von 15min Werte zu Stuendliche Werte rechnen
#for id in range(0,int(24/freq)):
#    ri = id/freq
#    # loop durch Winter daten
#    samW[id] = lastFactor*np.nansum(data1.sam_w[np.arange(ri, ri+1/freq)])/1000 # in kW
#    sonW[id] = lastFactor*np.nansum(data1.son_w[np.arange(ri, ri+1/freq)])/1000 # in kW
#    werkW[id] = lastFactor*np.nansum(data1.werk_w[np.arange(ri, ri+1/freq)])/1000 # in kW
#    # loop durch Somer daten
#    samS[id] = lastFactor*np.nansum(data1.sam_s[np.arange(ri, ri+1/freq)])/1000 # in kW
#    sonS[id] = lastFactor*np.nansum(data1.son_s[np.arange(ri, ri+1/freq)])/1000 # in kW
#    werkS[id] = lastFactor*np.nansum(data1.werk_s[np.arange(ri, ri+1/freq)])/1000 # in kW
#    # loop durch Uebergangszeit daten
#    samU[id] = lastFactor*np.nansum(data1.sam_u[np.arange(ri, ri+1/freq)])/1000 # in kW
#    sonU[id] = lastFactor*np.nansum(data1.son_u[np.arange(ri, ri+1/freq)])/1000 # in kW
#    werkU[id] = lastFactor*np.nansum(data1.werk_u[np.arange(ri, ri+1/freq)])/1000 # in kW

# Eigenverbrauch wenn der Zeitreihe fuer den gesamten Jahr nicht verfuegbar ist
#    ev_samW = np.fmin(Pel_h[k], np.ravel(samW[k]))
#    ev_sonW = np.fmin(Pel_h[k], np.ravel(sonW[k]))
#    ev_werkW = np.fmin(Pel_h[k], np.ravel(werkW[k]))
#    # minimum finden Sommer
#    ev_samS = np.fmin(Pel_h[k], np.ravel(samS[k]))
#    ev_sonS = np.fmin(Pel_h[k], np.ravel(sonS[k]))
#    ev_werkS = np.fmin(Pel_h[k], np.ravel(werkS[k]))
#    # minimum finden Ubergangszeit
#    ev_samU = np.fmin(Pel_h[k], np.ravel(samU[k]))
#    ev_sonU = np.fmin(Pel_h[k], np.ravel(sonU[k]))
#    ev_werkU = np.fmin(Pel_h[k], np.ravel(werkU[k]))
#
## summe von eingespieste energie in die stunden wo energie erzeugt wird
#ev_W = sum(ev_samW)*13 + sum(ev_sonW)*13 + sum(ev_werkW)*5*13
#ev_S = sum(ev_samS)*13 + sum(ev_sonS)*13 + sum(ev_werkS)*5*13
#ev_U = sum(ev_samU)*13 + sum(ev_sonU)*13 + sum(ev_werkU)*5*13
## eigenverbrauch im Jahr
#ev_J = ev_W + ev_S + ev_U

## Gesamtverbrauch vereinfachte mit gleich geteilte Saisonen
#gv_W = sum(samW)*13 + sum(sonW)*13 + sum(werkW)*5*13 # in Winter
#gv_S = sum(samS)*13 + sum(sonS)*13 + sum(werkS)*5*13 # in Sommer
#gv_U = sum(samU)*26 + sum(sonU)*26 + sum(werkU)*5*26 # in Ubergangszeit
## Gesamtverbrauch im Jahr
#gv_J = gv_S + gv_W + gv_U
## Autarkiegrad im Jahr
#Ag = ev_J/gv_J*100
## Einstrahlung [W/m2/a] = Stromertrag[kWh/a]/Anlagenleistung [kWp]
#pvN = 10 # Anlagen nennleistung [kWp]
#Ne = len(k)/24
#einS = Ne*Pel_J/pvN # Einstrahlung [W/m2/a] 

## kumulative Speicher kapazitaet mit linear assumption
# Sp = np.cumsum(dSp, axis=0)                   # basis Speicher kurve
# B = copy.copy(Sp[:]) # operative Speicher kurve

#            # Ladezyklus zaehlen
#            Ncyc[ik] = np.sum(np.diff(k_ll) > 1)
#            hi = Sp[:,ik]>=Sp0[ik]
#            lo_or_hi = (Sp[:,ik] <= 0) | hi
#            ind = np.nonzero(lo_or_hi)[0]
#            cnt = np.cumsum(lo_or_hi) # from 0 to len(x)
#            Ncyc[ik] = np.mean(sum(np.where(cnt, hi[ind[cnt-1]], False)))
#            if Ncyc[ik] > 365*0.75:
#                Ncyc[ik] = round(365*0.75)
