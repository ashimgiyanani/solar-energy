
"""
Created on Sun Oct  6 23:40:32 2019

@author: özgün
"""
# -*- coding: utf-8 -*-
"""

"""

import sys
import math
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time
import copy

import pathlib
import csv
# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

############################### DEBUG ########################################
DEBUG = False # Auf True setzenm, um Konsolenausgabe einzuschalten



# Bestimmung Anlagen- und Speichergröße
# Anfangswert für peak power in [kWp]
Pp = 30
# Multiplikationsfaktor fuer die Last
lastFactor=1 # , best combi  =2
# Multiplikationsfaktor fuer die Anlagengröße
pvFactor = np.array([5/30,10/30,1]) #arrays von 3 elementen
#pvFactor = np.array([5/30]) #arrays von 3 elementen
#PV Nennleistung wird angepasst
PV_kwp = pvFactor*Pp # Anlagengröße= 5, 10, 30
#Eingabe der Speichergröße in [kWh]
Sp0_1 = np.arange(0.1,1,0.1) # 0.1-0.9
Sp0_2 = np.arange(1,10,1) # 1-9
Sp0_3 = np.arange(10,110, 10) # 10-100
Sp0 = np.concatenate([Sp0_1, Sp0_2, Sp0_3], axis=0) #ein array von 3 arrays kombiniert


# Saule nummer im Sp0 matrix zu plotten, Sp0[plotSp] = plotSpeicher
plotSpeicher = 10  #speicherkapa.eingabe
plotSp = int(np.asarray(np.where(Sp0==plotSpeicher))) # benutzt Sp0[plotSp] = Sp0[3] zu plotten, eingaben von Sp0 werte fuer Funktionsweise Grafik


# parameter Zeitreihen Interval sich aendert [Stunden]
freq = 1 # Frequenz in Stunden, 1 Stunde = 1, 15 min = 1/4 Stunden
scale = 1.0 # scalefactor für increasing the Pel um den pvFactor
# Simulationsparameter fuer die Anzahl der Tage fuer plots und rechnungen. Fuer Kosten simulation nur365 Tage nutzen [Tage]
Nd = 365 # no. of days, '1' day for time series related, '3' day for speicher funktionsweise und '365' fuer Eigenverbrauch und Autarkiegrad zu rechnen


#Parametereingabe
preis_N_esp = 0.11 # 11cent/kWh
preis_N_ent = 0.29 # 29cent/kWh
kosten_PV = 1265*PV_kwp # [Euro/kWp]
Lebensdauer_Anlage = 20 # [Jahre]
Lebensdauer_batt = 15 # [Jahre]
#zyklenzahl Batterie fehlt
Nbatt = Lebensdauer_Anlage/Lebensdauer_batt #Batterieanzahl
kosten_Speicher = 800*Sp0*(Nbatt-1) # gesamte Speicherinvestition innerhalb Lebensdauer_Anlage 
kosten_Fix = 150 # jährliche Fixkosten [Euro/a]
Batterie = 1 # '1' fuer Batterie basierte rechnungen, '0' - ohne batterie 
saveFig = 0 # 0 - Bilder nicht speichern,  '1' - speichern
plotFig = 1 # 0 - Bilder nicht plotten,  '1' - plotten


askDate = '2013-10-13 00:00:00' # select the date from where Nd are plotted, for 3 days plotted dates are 13-10-2013 till 15-10-2013


#Einlesen der CSV-Dateien
# load profile einlesen
file1 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\load_profile_dots.txt" # Datei addresse auf windows
data1 = pd.read_csv(file1, sep='\t',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=5)

# convert to kWh, only if loads data given in kW/W 
time1 = pd.to_datetime(data1.time)
frac_kwh = (time1[1] -time1[0]) / np.timedelta64(1,'h')
data1.sam_w  = data1.sam_w/frac_kwh
data1.son_w  = data1.son_w/frac_kwh
data1.werk_w  = data1.werk_w/frac_kwh

data1.sam_s  = data1.sam_s/frac_kwh
data1.son_s  = data1.son_s/frac_kwh
data1.werk_s  = data1.werk_s/frac_kwh

data1.sam_u  = data1.sam_u/frac_kwh
data1.son_u  = data1.son_u/frac_kwh
data1.werk_u  = data1.werk_u/frac_kwh
 
# PV profile einlesen
file2 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\PV_profile_edited.csv" # Datei addresse auf windows
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0) 
# convert to kWh, only if PV data given in kW/W 
time2 = pd.to_datetime(data2.time)
frac_kwh_PV = (time2[1] -time2[0]) / np.timedelta64(1,'h')
data2.Pel_MW = data2.Pel_MW/frac_kwh_PV
data2.Pel_W = data2.Pel_W/frac_kwh_PV


# Zeitstempel mit 15-min  Interval
if (freq == 0.25):
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(15,'m'), dtype='datetime64'))
elif (freq==1):
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(1,'h'), dtype='datetime64'))
else:
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(1,'D'), dtype='datetime64'))
          
if (Nd==365):
   selectDate = int(0)
else:
   selectDate = int(np.asarray(np.where(tf == askDate)))


# Zeit vorstellungen
Nhr = int(Nd*24/freq) # Anzahl von Stunden oder 15min Werte
tt = np.linspace(1,Nhr,Nhr) # Zeitreihe je nach Eingabe Nd
yr = np.linspace(1,365,365*24/freq) # Jahr
dd = np.linspace(1,24,24/freq) # tag
wk = np.linspace(1,7,7*24/freq) # Woche
mon = np.linspace(1,30,30*24/freq) # Monat
hd = np.arange(1,Nd*(24/freq)+1)


# interpolation des PV-Profils (Pel_W)  15 minütig
Th = np.linspace(1,365,365*24) # Stuendliche Zeitstempel
T15m = np.linspace(1,365,365*24/freq) # 15 minute Zeitstempel
Pel_15m = np.interp(T15m,Th,data2.Pel_W[0:365*24]) # interpolierte 15 minute Pel Werte


# Taegliche mittelwerte
Td = np.linspace(1,365,365) # 1-day timestamp
Pel_1d  = np.zeros(Td.size)
for i in range(0,Td.size):
    ri = i*24 # Start indizien
    Pel_1d[i] = np.nansum(data2.Pel_W[np.arange(ri, ri+24)]) # Summe von Stuendliche Werte um einen Taegliche Mittelwert zu bauen

if (freq==0.25):
    Pel = copy.copy(Pel_15m)
elif (freq==1):
    Pel = copy.copy(data2.Pel_W)
else:
    Pel = copy.copy(Pel_1d)
        
        
# Initialization
data_Ag = np.zeros([Sp0.size,pvFactor.size])
data_evQ = np.zeros([Sp0.size,pvFactor.size])
data_N_esp = np.zeros([Sp0.size,pvFactor.size])
data_N_ent = np.zeros([Sp0.size,pvFactor.size])
data_Ncyc = np.zeros([Sp0.size,pvFactor.size])
data_kosten = np.zeros([Sp0.size,pvFactor.size])
data_dP = np.zeros([data2.Pel_W.size,pvFactor.size])
cumdata_N_esp = np.zeros([data2.Pel_W.size,Sp0.size,pvFactor.size])
cumdata_N_ent = np.zeros([data2.Pel_W.size,Sp0.size,pvFactor.size])


# Loop fuer mehrere pvFactor grosse
for iv in range(0,pvFactor.size):
    
    if DEBUG:
        print("Loop mit pvFactor = " + str(pvFactor[iv]))
    
    # Jahres PV profil mit Stuendliche daten, anpassung durch pvFactor
    Pel_h = pvFactor[iv]*Pel/1000 # Pel in [kW], Eingaben: Pel_15m,  Pel_1d, data2.Pel_W 
    
    
# Last profil fuer das gesamte Jahr, Initialization
    # Last im Winter
    samW = np.zeros([int(24/freq),1]) # Samstag Winter
    sonW = np.zeros([int(24/freq),1]) # Sonntag Winter
    werkW = np.zeros([int(24/freq),1]) # Werktag Winter
    # Last im Sommer
    samS = np.zeros([int(24/freq),1]) # Samstag Sommer
    sonS = np.zeros([int(24/freq),1]) # Sonntag Sommer
    werkS = np.zeros([int(24/freq),1]) # Werktag Sommer
    # Last in der Uebergangszeit
    samU = np.zeros([int(24/freq),1]) # Samstag Uebergangszeit
    sonU = np.zeros([int(24/freq),1]) # Sonntag Uebergangszeit
    werkU = np.zeros([int(24/freq),1]) # Werktag Uebergangszeit
    # Haushalt Energie Verbrauch Initialization fuer 3-tagige profil Vergleich
    Ver_h3d = np.zeros([int(24/freq)*3,1])


# Verbrauch von 15min Werte zu Stuendliche Werte rechnen
    for id in range(0,int(24/freq)):
        ri = (id*4)/freq
        # loop durch Winterdaten
        samW[id] = lastFactor*np.nansum(data1.sam_w[np.arange(ri, ri+4/freq)])/10000 # in kW, da punkt fehlt siehe file
        sonW[id] = lastFactor*np.nansum(data1.son_w[np.arange(ri, ri+4/freq)])/10000 # in kW
        werkW[id] = lastFactor*np.nansum(data1.werk_w[np.arange(ri, ri+4/freq)])/10000 # in kW
        # loop durch Sommerdaten
        samS[id] = lastFactor*np.nansum(data1.sam_s[np.arange(ri, ri+4/freq)])/10000 # in kW
        sonS[id] = lastFactor*np.nansum(data1.son_s[np.arange(ri, ri+4/freq)])/10000 # in kW
        werkS[id] = lastFactor*np.nansum(data1.werk_s[np.arange(ri, ri+4/freq)])/10000 # in kW
        # loop durch Uebergangszeitdaten
        samU[id] = lastFactor*np.nansum(data1.sam_u[np.arange(ri, ri+4/freq)])/10000 # in kW
        sonU[id] = lastFactor*np.nansum(data1.son_u[np.arange(ri, ri+4/freq)])/10000 # in kW
        werkU[id] = lastFactor*np.nansum(data1.werk_u[np.arange(ri, ri+4/freq)])/10000 # in kW
    
    
# indizien fuer die jahreszeiten
    # Winter 'a' - Jan-Maerz Beginn(1) und End(2) indicien
    tw1a = int(np.ravel(np.where(tf=='01-01-2013')))
    tw2a = int(np.ravel(np.where(tf=='21-03-2013'))) - 1
    # Uebergangszeit 'a' - Fruehling - Maerz-Mai, Beginn(1) und End(2) indicien
    tu1a = int(np.ravel(np.where(tf=='21-03-2013')))
    tu2a = int(np.ravel(np.where(tf=='15-05-2013'))) - 1
    # Sommerzeit, Beginn(1) und End(2) Zeit
    ts1 = int(np.ravel(np.where(tf=='15-05-2013')))
    ts2 = int(np.ravel(np.where(tf=='15-09-2013'))) - 1
    # Uebergangszeit 'b' - Herbst - Sept-Okt, Beginn(1) und End(2) indicien
    tu1b = int(np.ravel(np.where(tf=='15-09-2013')))
    tu2b = int(np.ravel(np.where(tf=='11-01-2013'))) - 1 #Amerikanisches Format, also 1. November 2013
    # Winter 'b' - Nov-Dez Beginn(1) und End(2) indicien
    tw1b = int(np.ravel(np.where(tf=='11-01-2013')))
    tw2b = len(tf) - 1
    '''
    if (freq==0.25):
        tw2b = int(np.ravel(np.where(tf=='31-12-2013 23:45:00')))
    elif (freq==1):
        tw2b = int(np.ravel(np.where(tf=='31-12-2013')))
    else:
        tw2b = int(np.ravel(np.where(tf=='31-12-2013')))
    '''


# Lastdaten in Wochen,jahreszeiten
# Wiederhole die Matrix in der form [5 x 1], um 5 WerkTage zu schaffen
    werkW_mat = np.tile(werkW,(5,1))
    werkS_mat = np.tile(werkS,(5,1))
    werkU_mat = np.tile(werkU,(5,1))
    # Matrix kombinieren in einer Woche
    week_w = np.vstack((werkW_mat,samW,sonW))
    week_s = np.vstack((werkS_mat,samS,sonS))
    week_u = np.vstack((werkU_mat,samU,sonU))
    # Wiederhole die Matrix zwischen Beginn und End Zeiten in der form [(tw2a-tw1a)/(168/freq) x 1] {PS: 168 = 24 Stunden* 7 Tage der Woche, freq - Stuendlich(1) oder 15 min (0.25)}
    Ver_wa = np.tile(week_w,(int(np.floor((tw2a-tw1a)/(168/freq))),1)) # winter 'a' Jan-Maerz
    Ver_ua = np.tile(week_u,(int(np.ceil((tu2a-tu1a)/(168/freq))),1)) # Uebergangszeit 'a' Fruehling - Maerz-Mai
    Ver_s = np.tile(week_s,(int(np.floor((ts2-ts1)/(168/freq))),1)) # sommerzeit Mai-Sept
    Ver_ub = np.tile(week_u,(int(np.ceil((tu2b-tu1b)/(168/freq))),1)) # Uebergangszeit 'b' Herbst - Sept-Okt
    Ver_wb = np.tile(week_w,(int(np.ceil((tw2b-tw1b)/(168/freq))),1)) # Winter 'b' Nov-Dez
    # jahreszeiten kombinieren in einem Jahr oder 1 Saison
    Ver_h = np.vstack((Ver_wa,Ver_ua,Ver_s,Ver_ub,Ver_wb,samW)).flatten() # ganzen Jahr {wegen der Rundung in 'int(np.floor((tw2a-tw1a)/(168/freq)))' fehlt ein Tag, ergaenzt mithilfe von samW}
    Ver_w = np.vstack((Ver_wb,samW,Ver_wa)).flatten() # Winter 'a' und Winter 'b' kombiniert
    Ver_u = np.vstack((Ver_ua,Ver_ub)).flatten() # Uebergangszeit 'a' und 'b' kombiniert
    Ver_s = Ver_s.flatten() # die Matrix flach machen


# PV leistung waehrend den jahreszeiten
    Pel_wb = Pel_h[tw1b:tw2b] # Winter 'b' PV Leistung
    Pel_wa = Pel_h[tw1a:tw2a] # Winter 'b' PV Leistung
    Pel_ua = Pel_h[tu1a:tu2a] # Uebergangszeit 'a' PV Leistung
    Pel_ub = Pel_h[tu1b:tu2b] # Uebergangszeit 'b' PV Leistung
    Pel_s = Pel_h[ts1:ts2]    # Sommerzeit PV Leistung
    Pel_w = np.concatenate((Pel_wb,Pel_wa)) # PV Winter 'a' und 'b' kombiniert
    Pel_u = np.concatenate((Pel_ua,Pel_ub)) # PV Uebergangszeit 'a' und 'b' kombiniert
    
   
    
    if(iv == 10/30):
        # PV Plot
        if (plotFig==1):
            plt.figure()
            plt.plot(hd/24, Pel_h[:]) #fuer Zeit: T15m, Td oder Th, für Nennleistung: Pel_15m, Pel_1d oder Pel_h
            plt.xlabel('Tage')
            plt.ylabel('Leistung (kW)')
            plt.title('Datenprofil der Photovoltaikanlage - Jahresansicht')
            plt.show()
            if (saveFig==1):
                plt.savefig('Jahresansicht_PV_TaeglicheWerte.png')
                
        # PV Plot je nach Saison  (ua=Frühling)(ub=Herbst)
        tt1 = np.linspace(0,Pel_s.size/(24/freq),Pel_s.size) #Eingabe! siehe unten
        if (plotFig==1):
            plt.figure()
            plt.plot(tt1,Pel_s) # eingabe hier, Nennleistung: Pel_w, Pel_s oder Pel_u, Pel_ua, Pel_ub, Pel_wa, Pel_wb
            plt.xlabel('Zeit (Tage)')
            plt.ylabel('Leistung (kW)')
            plt.title('Datenprofil der Photovoltaikanlage - Sommer')
            plt.show()
            if (saveFig==1):
                plt.savefig('Sommer_PV.png')
    
    
# Last Plot
    if (plotFig==1) & (iv==0):
        plt.figure()
        plt.plot(tt,Ver_h[0:Nhr]) # eingabe:[0:Nhr] -> [start:ende]; z.B. start = np.where(tf=='2013-10-03'), end = np.where(tf=='2013-10-05')
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Verbrauch (kW)')
        plt.title('Datenprofil Last - Jahresansicht')
        # anpassen der xticks auf passende Zeit
        plt.axis('tight')
        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
        ax.set_xticks(custom_ticks)
        ax.set_xticklabels(np.rint(custom_ticks/(24))) # 24=365
        plt.show()
        if (saveFig==1):
            plt.savefig('Jahresansicht_Last.png')
    
# Last Plot nach Saison  (ua=Frühling ub=Herbst)
    tt1 = np.linspace(0,Ver_s.size/(24/freq),Ver_s.size) #Eingabe! siehe unten
    if (plotFig==1):
        plt.figure()
        plt.plot(tt1,Ver_s) # eigabe hier: Ver_w, Ver_u, Ver_s, Ver_ua, Ver_ub, Ver_wb, Ver_wa, auch bei tt1 3 Reihen oben
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Verbrauch (kW)')
        plt.title('Datenprofil Verbrauch - Sommer')
        plt.show()
        if (saveFig==1):
            plt.savefig('Sommer_Last.png')
    
#-----------------------------------------------------------------------------------------------------
    
# Verbrauch mit PV energie    
    Pel_hd = scale*Pel_h[selectDate:selectDate+Nd*int(24/freq)*4] # 1 Tag im Winter-Januar, selectDate=wo der Pel_hd beginnt
    if (Nd==3): # fuer 3 tag Profil
        Ver_hd = np.concatenate([samW,sonW,werkW], axis=0)
    elif (Nd==1): # fuer 1 Tag profil
        Ver_hd = samW
    elif (Nd==365): # fuer 1 Jahr Profil
        Ver_hd = Ver_h
    else:
        print("Nd is negative")
        
   
#Algorithmus des Speichers
    # Unterschied zwischen PV und Last
    dP = (np.asarray(Pel_hd[:]) - np.ravel(Ver_hd)) # dP/dt slope of the curve 
    data_dP[:,iv] = copy.copy(dP)
    # Unterschied samt Speicherkapazitaet
    nr = dP.size
    nc = Sp0.size
    Sp = np.zeros([nr,nc])
    Ncyc = np.zeros(nc)
    # loop mit mehrere Speichergroessen
    #for j in range(20,Sp0.size):
    for j in range(0,Sp0.size):    
        count = int(0)
        S = np.zeros(nr)
        S_MAX = Sp0[j]   # MAXIMALE SPEICHERKAPAZITÄT
        S[0] = 0    # Speicher Anfangswert
        
        if DEBUG:
            print("-> Berechne Speicherreihe mit pvFactor = " + str(pvFactor[iv]) + " und Sp0 = " + str(Sp0[j]) + " S_MAX= " + str(S_MAX))
        
        # loop mit Zeitreihe grosse
        for i in range(1, dP.size):
            if (dP[i] <= 0) & (S[i-1] <= 0): # Netzbezug
                S[i] = 0
            elif (dP[i] <= 0) & (S[i-1] > 0): # dP = P-V -ve, aber Speicher noch nicht leer
                S[i] = S[i-1] + dP[i]
                if (S[i-1]==S_MAX) & (i!=1): # loop fuer Ladezyklen
                    count = count+1 # ladezyklen + 1
            elif ((dP[i] > 0) & (S[i-1] <= 0)): # dP +ve, Speicher leer
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] >= 0) & (S[i-1] < S_MAX):# dP +ve, Speicher ladet
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] >= S_MAX): # dP +ve, Speicherkapazitaet erreicht
                S[i] = S_MAX
            else:
                print('something went wrong at i=' + str(i) + " dP[i]=" + str(dP[i]) + " S[i]=" + str(S[i]))
            
            if DEBUG:
                print("i = " + str(i) +  " S_MAX = " + str(S_MAX) + " Speicher = " + str(S[i]))
        #S[S<0] = 0 # Speicherwerte waehrend Netzbezug
        #S[S>S[0]] = S_MAX # Speicherwerte waehrend Netzeinspeisung
        for s_iter in range(0, S.size):
            if (S[s_iter] > S_MAX):
                S[s_iter] = S_MAX
            elif (S[s_iter] < 0):
                S[s_iter] = 0
        #plt.figure()
        #plt.plot(S)

                
        Sp[:,j] = copy.copy(S) 
        Ncyc[j] = count 
        
#    plt.figure()
#    plt.plot(Sp)
#    plt.title("pvFactor = " + str(pvFactor[iv]))
#    plt.legend(loc=0)
    
    
    # Speicher behaviour at full and no load condition
    N_esp = np.zeros([nr, nc])
    N_ent = np.zeros([nr, nc])
    Splus = np.zeros([nr, nc])
    Sminus = np.zeros([nr,nc])
    cum_N_esp = np.zeros([nr,nc])
    cum_N_ent = np.zeros([nr,nc])
    for ik in range(0,Sp0.size):
        if DEBUG:
            print("--> Berechne Indizes für Sp0 = " + str(Sp0[ik]))
        k_ul = np.ravel(np.where(Sp[:,ik]==Sp0[ik])) # speicher voll index
        k_ll = np.ravel(np.where(Sp[:,ik] == 0)) # speicher leer index
        k_plus = np.ravel(np.where((dP>0) & (Sp[:,ik] < Sp0[ik]))) # speicher ladung index
        k_minus = np.ravel(np.where((dP<=0) & (Sp[:,ik]> 0))) # speicher entladung index
        k_esp = np.ravel(np.where((Sp[:,ik]==Sp0[ik]) & (dP>0))) # netz einspeisen index
        k_ent = np.ravel(np.where((Sp[:,ik]==0) & (dP<0))) # netz entnehmen index
        # Speicherladung und entladung
        Splus[k_plus, ik] = dP[k_plus] # speicherladungswerte
        Sminus[k_minus, ik] = dP[k_minus] # speicherentladungswerte
        # Netzeinspeisung und entnehmen
        N_esp[k_esp,ik] = dP[k_esp] # energie ins netz eingespeist
        N_ent[k_ent,ik] = dP[k_ent] # energie von netz entnommen
        # kumulative Netzeinspeisung und Netzbezugswerte
        cum_N_esp[:,ik] = np.cumsum(N_esp[:,ik])
        cum_N_ent[:,ik] = np.cumsum(N_ent[:,ik])            



    # Treppenfunktion fuer parameter
    cumPel_h = np.cumsum(Pel_h)
    cumVer_h = np.cumsum(Ver_h)
    # kumulative Daten  
    cumdata_N_esp[:,:,iv] = copy.copy(cum_N_esp)         
    cumdata_N_ent[:,:,iv] = copy.copy(cum_N_ent) 
     
    
# Speicherfunktionsweise Plot      
    if (plotFig==1):
        fig,ax1 = plt.subplots()
        ax2 = ax1.twinx()
        f1 = ax1.plot(hd/24, Ver_hd,'r-',label="Verbrauch [kWh]")
        f2 = ax1.plot(hd/24, Pel_hd, 'b-',label="PV [kWh]")
        f3 = ax2.plot(hd/24, Sp[:,plotSp], 'g-',label="Speicher[kWh]")
        #plt.plot(hd/24, Sp[1:],'r.', label='og')
#        plt.plot(hd/24,N_esp[:,plotSp],'b.',label='N_esp')
#        plt.plot(hd/24,N_ent[:,plotSp],'g.', label='N_ent')
#        ax1.legend(loc=1)
        ax1.set_xlabel('Zeit (Tage)')
        ax1.set_ylabel('Leistung (kW)', color='b')
        ax2.set_ylabel('Speicher (kWh)', color='g')
        plt.title('Funktionsweise-Speicher')
        # anpassen die xticks auf passende Zeit
#        ax = plt.gca()
#        xmin, xmax = ax.get_xlim()
#        custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
#        ax.set_xticks(custom_ticks)
#        ax.set_xticklabels(np.round(custom_ticks/(4*24)))
        # set the legends in one legend property
        l1,leg1 = ax1.get_legend_handles_labels()
        l2,leg2 = ax2.get_legend_handles_labels()
        ax2.legend(l1+l2,leg1+leg2,loc=1)
        if (saveFig==1):
            plt.savefig('Speicher_funktionsweise.png')
    
    
# Energie Plot
    if (plotFig==1):
        plt.figure()
        plt.plot(hd/24,cum_N_esp[:,plotSp],'m.-',label='Netzeinspeisung')
        plt.plot(hd/24, abs(cum_N_ent[:,plotSp]),'k.-', label = 'Netzbezug')
        plt.plot(hd/24, abs(cumPel_h),'g.-', label = 'Erzeugung')
        plt.plot(hd/24, abs(cumVer_h),'b.-', label = 'Verbrauch')
        plt.legend(loc=0)
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Energie (kWh)')
        plt.title('Energie - Jahresansicht ' + str(PV_kwp[iv]) + ' kWp' + ", Batteriegröße: " + str(plotSpeicher) + "kWh")
        if (saveFig==1):
            plt.savefig('Energie_Jahresansicht.png')
            
# Energie Plot fuer 3-Tage       
    if (plotFig==1):
        plt.figure()
        plt.plot(hd[0:72]/24,cum_N_esp[0:72,0],'m.-',label='Netzeinspeisung')
        plt.plot(hd[0:72]/24, abs(cum_N_ent[0:72,0]),'k.-', label = 'Netzbezug')
        plt.plot(hd[0:72]/24, abs(cumPel_h[0:72]),'g.-', label = 'Erzeugung')
        plt.plot(hd[0:72]/24, abs(cumVer_h[0:72]),'b.-', label = 'Verbrauch')
        plt.legend(loc=0)
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Energie (kWh)')
        plt.title('Energie_3_Tageansicht ' + str(PV_kwp[iv]) + ' kWp' + ", Batteriegröße: " + str(plotSpeicher) + "kWh")
        if (saveFig==1):
            plt.savefig('Energie_treppenfunktion.png')


#------------------------------------------------------------------------------------------------------------------
   
# Initialization für Berechnungen der Betriebsgrößen
    dv_J = np.zeros([nc]) # Direktverbrauchte PV-Energie im Jahr
    evQ = np.zeros([nc]) # eigenverbrauchsquote
    Ag = np.zeros([nc]) # Autarkiegrad
    Pel_J = sum(Pel_h) # EPV


# Start Berechnungen(Autarkie + Eigenverbrauchsquote) 
#mit Batterie
    if Batterie==1:     
        for ik in range(0,Sp0.size):
            #Direktverbrauch= ELast - EPV
            dv_J=  sum(np.fmin(Pel_h, np.ravel(Ver_h)))
            #Eigenverbrauchsquote= (EDirektverbrauch + EBatterieladung) /(EPV) *100
            evQ[ik] = ((dv_J + abs(sum(Splus[:,ik])))/Pel_J)*100
            #Autarkiegrad = (EDirektverbrauch + EBatterieentladung) /(ELast) * 100
            Ag[ik] = ((dv_J + abs(sum(Sminus[:,ik])))/(sum(Ver_h)))*100
# Ohne Batterie    
    else:
        ip  = [i for (i,val) in enumerate(Pel_h) if val!=0]
        for ik in range(0,Sp0.size):
            #Direktverbrauch= ELast - EPV
            dv_J[ik]= sum(np.fmin(Pel_h[ip], np.ravel(Ver_h[ip])))
            #Eigenverbrauchsquote= (ELast-EPV)/EPV * 100
            evQ[ik] = (dv_J[ik]/Pel_J)*100
            # Autarkiegrad= (ELast - EPV)/ELast * 100
            Ag[ik]= (dv_J[ik]/sum(Ver_h))*100 
   
    
    # loop parameter, kombinieren wegen PV Nennleistung werte in pvFactor     
    data_evQ[:,iv] = copy.copy(evQ)
    data_Ag[:,iv] = copy.copy(Ag)
    data_N_esp[:,iv] = copy.copy(np.sum(N_esp,axis=0))
    data_N_ent[:,iv] = copy.copy(np.sum(N_ent,axis=0))
    data_Ncyc[:,iv] = copy.copy(Ncyc)
    data_kosten[:,iv] = kosten_Speicher + kosten_PV[iv]
    

    
#Start Berechnungen der Wirtschaftlichen Größen mit Speicher (Gewinn/Verlust, Amortisation)
    #Amortisation   
    #Summe Jahreseinnahmen (welche Speichegröße eingespeist)
    Erloese= abs(data_N_esp)*preis_N_esp + (sum(Ver_h)*preis_N_ent*evQ[ik]/100) - kosten_Fix
    #Summe Jahresausgaben (Speicherkosten + Anlagekosten)
    Kosten_A = (np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))
    #Abschluss
    Amortisation = Kosten_A/Erloese
   
    #Gewinn/Verlust auf ein Jahr runtergerechnet
    Kosten_G = ((np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))/Lebensdauer_Anlage) + abs(data_N_ent)*preis_N_ent
    #Abschluss
    Gewinn = Erloese - Kosten_G
    
    #ohne Batterie rechnungen fehlen


#Diagramme Plotten
#Eigenverbrauchsquote
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_evQ)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Eigenverbrauchsquote (%)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Eigenverbrauchsquote_BatterieGr.png')

# Autarkiegrad
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_Ag)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Autarkiegrad (%)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Autarkiegrad_BatterieGr.png')

# Energie aus dem Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_ent))
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Netzbezug (kWh/a)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzBezug_BatterieGr.png')

# Einspeisestrom
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_N_esp)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Netzeinspeisung (kWh/a)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzEinspeisen_BatterieGr.png')

# Ladezyklen
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_Ncyc)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Ladezyklen (jährliche Anzahl)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Ladezyklen.png')

# Kosten  Energie aus dem Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_ent)*preis_N_ent)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Netzbezugskosten (€/a)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzBezug_preis.png')

# Einspeiseverguetung  Energie ins Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_esp)*preis_N_esp)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Einspeisevergütung (€/a)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzEinspeisen_preis.png')



# Amortisation
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0, Amortisation)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Amortisation (Jahre)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Amortisation.png')

# Gewinn
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0, Gewinn)
    plt.grid(True)
    plt.xlabel('Batteriegröße (kWh)')
    plt.ylabel('Gewinn (€/a)')
    plt.legend(['5 kWp', '10 kWp', '20 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Gewinn.png')