
import csv
import numpy as np
import matplotlib.pyplot as plt
import math
from copy import deepcopy



# Code starts from here

with open('./PV_profile_csv1.csv',encoding='mac_roman') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    row_count = sum(1 for row in csv_reader)
    csv_file.closed
row_count=row_count-5
with open('./PV_profile_csv1.csv',encoding='mac_roman') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0
    time=[]
    power_float=[]
    power=[]
    for row in csv_reader:
        if line_count >1:  # data lines
            time.append(row[0])
            power.append(row[2]) 
        line_count+=1    
csv_file.closed
print(row_count)
for i in range(len(power)):
    temp=float(power[i].replace(",",".")) #replace comma with dot for decimal point and convert it to float
    power_float.append(temp)

  
# ploting and saving the data
plot1=plt.figure()
plt.plot(time[0:row_count:1],power_float[0:row_count:1])
plt.xlabel('Date')
plt.ylabel('Power[kW]')
plt.box('on')
plt.grid(b=1,axis='both')


plt.figlegend(('Original Plot'))
plt.show()




