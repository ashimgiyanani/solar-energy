# -*- coding: utf-8 -*-
"""
@file: %(file)
@description: 
@syntax:
@inputs:

@outputs:
@example:

See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

@Author: %(username), PhD candidate, TU Delft
Wind energy department, TU Delft
email address: ashimgiyanani@yahoo.com
March 2018; Last revision: 06-04-2018
Created on %(date)s
@version: 0.1
"""

import sys
import math
import fnmatch, re
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time

import glob
import fnmatch
import pathlib
import os
from urllib.parse import urlparse
import csv
import copy

# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# read load profile
file1 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\load_profile1.csv"
data1 = pd.read_csv(file1, sep=',',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=4)

# read annual PV profile
file2 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\PV_profile_edited.csv"
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0)

tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(15,'m'), dtype='datetime64'))
Th = np.linspace(1,365,365*24)
T15m = np.linspace(1,365,365*24*4)
Pel_15m = np.interp(T15m,Th,data2.Pel_W)

P = Pel_15m[0:96]
V = data1.sam_w
P = Pel_15m[0:35040-96]
V_week = np.concatenate([np.array([data1.sam_u]), np.array([data1.son_u]), np.tile(data1.werk_u,[5,1])], axis=0).T
V = np.tile(np.ravel(V_week,order='F'),52)

S = np.zeros(P.shape)
S[0] = 10000
dP = P[:]-V[:]
for i in range(1,dP.size):
    if (dP[i] < 0) & (S[i-1] <= 0):
        S[i] = 0
    elif (dP[i] < 0) & (S[i-1] > 0):
        S[i] = S[i-1] + dP[i]
    elif ((dP[i] > 0) & (S[i-1] <= 0)):
        S[i] = dP[i] + S[i-1]
    elif (dP[i] > 0) & (S[i-1] > 0) & (S[i-1] < S[0]):
        S[i] = dP[i] + S[i-1]
    elif (dP[i] > 0) & (S[i-1] >= S[0]):
        S[i] = S[0]
    else:
        print('something went wrong at i=',int(i))
S[S<0] = 0
S[S>S[0]] = S[0]


plt.figure()
plt.plot(P,'b-', label="P")
plt.plot(V,'r-', label="V")
plt.plot(dP,'k.', label="dP")
plt.plot(S,'m.-', label="S")
plt.legend()

sys.exit("manual stop 1")


S[dP<0] = 0



        


B = np.zeros(P.shape)
Bplus = np.zeros([96,1]).flatten()
Bminus = np.zeros([96,1]).flatten()
N_esp = np.zeros([96,1]).flatten()
N_ent = np.zeros([96,1]).flatten()
    
k_ul = np.ravel([np.where(S > S0)])
k_ll = np.ravel(np.where(S < 0))
k_plus = np.ravel(np.where((dP>0) & (S <= S0)))
k_minus = np.ravel(np.where((dP<0) & (S > 0)))
k_esp = np.ravel(np.where((dP>0) & (S>S0)))
k_ent = np.ravel(np.where((dP<0) & (S<=0)))

S[k_ul] = copy.copy(S0)
S[k_ll] = 0
Bplus[k_plus] = dP[k_plus]
Bminus[k_minus] = dP[k_minus]
N_esp[k_esp] = dP[k_esp]
N_ent[k_ent] = dP[k_ent]


# Algorithm for charging and discharging of the battery
dP_temp = copy.copy(dP)
dP_temp[dP<0] = 0
S = np.cumsum(dP,axis=0)
dP_temp[(dP<0) & (S>0)] = dP[(dP<0) & (S>0)]
S_temp = np.cumsum(dP_temp,axis=0)
dP_temp[(dP>0) & (S_temp>=S0)] = 0
S_temp2 = np.cumsum(dP_temp,axis=0)
S_temp2[(S_temp2<0)] = 0 
