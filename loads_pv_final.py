# -*- coding: utf-8 -*-
"""

"""

import sys
import math
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time
import copy

import pathlib
import csv
# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Eingabe parameter
# peak power in [kWp]
Pp = 15
# Multiplikations Factor fuer Verbrauch lasten [-]
lastFactor=3 # , best combi  =2
# Multiplication Factor fuer PV leistung [-]
pvFactor = np.array([5/15,10/15,1]) # hier ist einen array von 3 elementen
#pvFactor = np.array([10/15]) # hier ist einen array von 1 element zum Probieren Zweck
# Aktuelle PV Nennleistung nach Anpassung durch der Multiplikations faktor
PV_kwp = pvFactor*Pp
# Speicher Kapazitaet Eingabe in [kWh]
Sp0_1 = np.arange(0.1,1,0.1) # Speicher kapazitaet Eingabe in kWh
Sp0_2 = np.arange(1,10,1)
Sp0_3 = np.arange(10,110, 10)
Sp0_4 = np.arange(100,1000,100)
Sp0 = np.concatenate([Sp0_1, Sp0_2, Sp0_3, Sp0_4], axis=0) # hier ist einen array von 3 arrays kombiniert
#Sp0 = np.linspace(0,1010,10)
#Sp0 = np.array([10]) # hier ist einen array von 1 element zum probieren Zweck
# Saule nummer im Sp0 matrix zu plotten, Sp0[plotSp] = plotSpeicher
plotSpeicher = 10
plotSp = int(np.asarray(np.where(Sp0==plotSpeicher))) # benutzt Sp0[plotSp] = Sp0[3] zu plotten, eingaben von Sp0 werte fuer Funktionsweise Grafik
# parameter wenn die Zeitreihen Interval aendert [Stunden]
freq = 1 # Frequenz in Stunden, 1 Stunde = 1, 15 min = 1/4 Stunden
scale = 1.0 # scale factor for increasing the Pel to optimize the pvFactor
# Simulationsparameter fuer die Anzahl der Tage fuer plots und rechnungen. Fuer Kosten simulation nur den 365 Tage nutzen [Tage]
Nd = 365 # no. of days, '1' day for time series related, '3' day for speicher funcktionsweise und '365' fuer Eigenverbrauch und Autarkiesgrad zu rechnen
preis_N_esp = 11 # 11c/kWh
preis_N_ent = 29 # 29c/kWh
kosten_PV = 1265*PV_kwp # [Eur/kWp]
Lebensdauer_Anlage = 20 # no.of years [Jahre]
Lebensdauer_batt = 15 # no. of years [Jahre]
Nbatt = Lebensdauer_Anlage/Lebensdauer_batt # Anzahl Batterien [-]
kosten_Speicher = 800*Sp0*(Nbatt-1) # Speicher Kosten [Eur/kWh/a]
kosten_Fix = 150 # PV Fix Kosten [Eur]
Batterie = 1 # '1' fuer Batterie basierte rechnungen, '0' - fuer ohne batterie installation
saveFig = 0 # 0 - Grafik Bilder nicht speichern,  '1' - speichern
plotFig = 1 # 0 - Grafik Bilder nicht plotten,  '1' - plotten
askDate = '2013-10-13 00:00:00' # select the date from where Nd are plotted, for 3 days plotted dates are 13-10-2013 till 15-10-2013

# load profile einlesen
file1 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\load_profile_dots.txt" # Datei addresse auf windows
data1 = pd.read_csv(file1, sep='\t',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=5) # csv daten einlesen, names - Die Name von den Spalten

# read annual PV profile
file2 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\PV_profile_edited.csv" # Datei addresse auf windows
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0) # csv daten einlesen, fuer details pandas documentation lesen

# Zeitstempel mit 15-min  Interval
if (freq == 0.25):
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(15,'m'), dtype='datetime64'))
elif (freq==1):
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(1,'h'), dtype='datetime64'))
else:
    tf = pd.to_datetime(np.arange('2013-01-01 00:00:00', '2014-01-01 00:00:00', np.timedelta64(1,'D'), dtype='datetime64'))
            
if (Nd==365):
   selectDate = int(0)
else:
   selectDate = int(np.asarray(np.where(tf == askDate)))

# Zeit vorstellungen
Nhr = int(Nd*24/freq) # Anzahl von Stunden oder 15min Werte
tt = np.linspace(1,Nhr,Nhr) # Zeitreihe je nach der Eingabe Nd
yr = np.linspace(1,365,365*24/freq) # Jahr
dd = np.linspace(1,24,24/freq) # tag
wk = np.linspace(1,7,7*24/freq) # Woche
mon = np.linspace(1,30,30*24/freq) # Monat
hd = np.arange(1,Nd*(24/freq)+1)

# interpolation um die Pel_W datei in 15 min zu rechnen
Th = np.linspace(1,365,365*24) # Stuendliche Zeitstempel
T15m = np.linspace(1,365,365*24/freq) # 15 minute Zeitstempel
Pel_15m = np.interp(T15m,Th,data2.Pel_W[0:365*24]) # interpolierte 15 minute Pel Werte

# Taegliche mittelwerte
Td = np.linspace(1,365,365) # 1-day timestamp
Pel_1d  = np.zeros(Td.size)
for i in range(0,Td.size):
    ri = i*24 # Start indicien
    Pel_1d[i] = np.nansum(data2.Pel_W[np.arange(ri, ri+24)]) # Summe von Stuendliche Werte um eine Taegliche Mittelwert zu bauen

if (freq==0.25):
    Pel = copy.copy(Pel_15m)
elif (freq==1):
    Pel = copy.copy(data2.Pel_W)
else:
    Pel = copy.copy(Pel_1d)
        
        
# Initialization
data_Ag = np.zeros([Sp0.size,pvFactor.size])
data_evQ = np.zeros([Sp0.size,pvFactor.size])
data_N_esp = np.zeros([Sp0.size,pvFactor.size])
data_N_ent = np.zeros([Sp0.size,pvFactor.size])
data_Ncyc = np.zeros([Sp0.size,pvFactor.size])
data_kosten = np.zeros([Sp0.size,pvFactor.size])
data_dP = np.zeros([data2.Pel_W.size,pvFactor.size])
# Loop fuer mehrere pvFactor grosse
for iv in range(0,pvFactor.size):
    # Jahres PV profil mit Stundliche daten, anpassung durch pvFactor
    Pel_h = pvFactor[iv]*Pel/1000 # Pel in [kW], Eingabe: Pel_15m kann geasendert werden, alternativen Pel_1d (Taegliche mittelwerte). data2.Pel_W (Stuendliche Mittelwerte)
    
    # Last profil fuer den gesamten Jahr, Initialization
    # Verbrauch stuendliche Mittelwerte in einem Tag im Winter
    samW = np.zeros([int(24/freq),1]) # Samstag Winter
    sonW = np.zeros([int(24/freq),1]) # Sonntag Winter
    werkW = np.zeros([int(24/freq),1]) # Werktag Winter
    # Tagesverbrauch Haushalt im Sommer
    samS = np.zeros([int(24/freq),1]) # Samstag Sommer
    sonS = np.zeros([int(24/freq),1]) # Sonntag Sommer
    werkS = np.zeros([int(24/freq),1]) # Werktag Sommer
    # Tages Eigenvarbrauch Haushalt im Uebergangszeit
    samU = np.zeros([int(24/freq),1]) # Samstag Uebergangszeit
    sonU = np.zeros([int(24/freq),1]) # Sonntag Uebergangszeit
    werkU = np.zeros([int(24/freq),1]) # Werktag Uebergangszeit
    # Haushalt Energie Verbrauch Initialization fuer 3-tagige profil Vergleich
    Ver_h3d = np.zeros([int(24/freq)*3,1])
    # Tages Profil mit 15-min daten, anpassung durch lastFactor
#    if (freq==0.25):
#            samW = lastFactor*data1.sam_w/1000 # [kW]
#            sonW = lastFactor*data1.son_w/1000 #  [kW]
#            werkW = lastFactor*data1.werk_w/1000 #  [kW]
#            samS = lastFactor*data1.sam_s/1000 #  [kW]
#            sonS = lastFactor*data1.son_s/1000 #  [kW]
#            werkS = lastFactor*data1.werk_s/1000 #  [kW]
#            samU = lastFactor*data1.sam_u/1000 #  [kW]
#            sonU = lastFactor*data1.son_u/1000 #  [kW]
#            werkU = lastFactor*data1.werk_u/1000 #  [kW]
#    elif (freq==1):
        # Verbrauch von 15min Werte zu Stuendliche Werte rechnen
    for id in range(0,int(24/freq)):
        ri = id/freq
        # loop durch Winter daten
        samW[id] = lastFactor*np.nansum(data1.sam_w[np.arange(ri, ri+4/freq)])/1000 # in kW
        sonW[id] = lastFactor*np.nansum(data1.son_w[np.arange(ri, ri+4/freq)])/1000 # in kW
        werkW[id] = lastFactor*np.nansum(data1.werk_w[np.arange(ri, ri+4/freq)])/1000 # in kW
        # loop durch Somer daten
        samS[id] = lastFactor*np.nansum(data1.sam_s[np.arange(ri, ri+4/freq)])/1000 # in kW
        sonS[id] = lastFactor*np.nansum(data1.son_s[np.arange(ri, ri+4/freq)])/1000 # in kW
        werkS[id] = lastFactor*np.nansum(data1.werk_s[np.arange(ri, ri+4/freq)])/1000 # in kW
        # loop durch Uebergangszeit daten
        samU[id] = lastFactor*np.nansum(data1.sam_u[np.arange(ri, ri+4/freq)])/1000 # in kW
        sonU[id] = lastFactor*np.nansum(data1.son_u[np.arange(ri, ri+4/freq)])/1000 # in kW
        werkU[id] = lastFactor*np.nansum(data1.werk_u[np.arange(ri, ri+4/freq)])/1000 # in kW
    
    # indicien fuer die Saisonen
    # Winter 'a' - Jan-Maerz Beginn(1) und End(2) indicien
    tw1a = int(np.ravel(np.where(tf=='01-01-2013')))
    tw2a = int(np.ravel(np.where(tf=='21-03-2013'))) - 1
    # Uebergangszeit 'a' - Fruehling - Maerz-Mai, Beginn(1) und End(2) indicien
    tu1a = int(np.ravel(np.where(tf=='21-03-2013')))
    tu2a = int(np.ravel(np.where(tf=='15-05-2013'))) - 1
    # Sommerzeit, Beginn(1) und End(2) Zeit
    ts1 = int(np.ravel(np.where(tf=='15-05-2013')))
    ts2 = int(np.ravel(np.where(tf=='15-09-2013'))) - 1
    # Uebergangszeit 'b' - Herbst - Sept-Okt, Beginn(1) und End(2) indicien
    tu1b = int(np.ravel(np.where(tf=='15-09-2013')))
    tu2b = int(np.ravel(np.where(tf=='11-01-2013'))) - 1
    # Winter 'b' - Nov-Dez Beginn(1) und End(2) indicien
    tw1b = int(np.ravel(np.where(tf=='11-01-2013')))
    if (freq==0.25):
        tw2b = int(np.ravel(np.where(tf=='31-12-2013 23:45:00')))
    elif (freq==1):
        tw2b = int(np.ravel(np.where(tf=='31-12-2013')))
    else:
        tw2b = int(np.ravel(np.where(tf=='31-12-2013')))


    # haushalt Verbrauch Daten in Wochen,Saisonen
    # Wiederhole die Matrix in der form [5 x 1], um 5 WerkTage zu schaffen
    werkW_mat = np.tile(werkW,(5,1))
    werkS_mat = np.tile(werkS,(5,1))
    werkU_mat = np.tile(werkU,(5,1))
    # Matrix kombinieren in einer Woche
    week_w = np.vstack((werkW_mat,samW,sonW))
    week_s = np.vstack((werkS_mat,samS,sonS))
    week_u = np.vstack((werkU_mat,samU,sonU))
    # Wiederhole die Matrix zwischen Beginn und End Zeiten in der form [(tw2a-tw1a)/(168/freq) x 1] {PS: 168 = 24 Stunden* 7 Tage der Woche, freq - Stuendlich(1) oder 15 min (0.25)}
    Ver_wa = np.tile(week_w,(int(np.floor((tw2a-tw1a)/(168/freq))),1)) # winter 'a' Jan-Maerz
    Ver_ua = np.tile(week_u,(int(np.ceil((tu2a-tu1a)/(168/freq))),1)) # Uebergangszeit 'a' Fruehling - Maerz-Mai
    Ver_s = np.tile(week_s,(int(np.floor((ts2-ts1)/(168/freq))),1)) # sommerzeit Mai-Sept
    Ver_ub = np.tile(week_u,(int(np.ceil((tu2b-tu1b)/(168/freq))),1)) # Uebergangszeit 'b' Herbst - Sept-Okt
    Ver_wb = np.tile(week_w,(int(np.ceil((tw2b-tw1b)/(168/freq))),1)) # Winter 'b' Nov-Dez
    # Saisonen kombinieren in einem Jahr oder 1 Saison
    Ver_h = np.vstack((Ver_wa,Ver_ua,Ver_s,Ver_ub,Ver_wb,samW)).flatten() # ganzen Jahr {PS:wegen der Rundung in 'int(np.floor((tw2a-tw1a)/(168/freq)))' fehlt einen Tag, habe ergaenzt mithilfe von samW}
    Ver_w = np.vstack((Ver_wb,samW,Ver_wa)).flatten() # Winter 'a' und Winter 'b' kombiniert
    Ver_u = np.vstack((Ver_ua,Ver_ub)).flatten() # Uebergangszeit 'a' und 'b' kombiniert
    Ver_s = Ver_s.flatten() # die Matrix flach machen


    # PV Leistung daten in Saisonen
    # PV leistung waehrend der Saisonen
    Pel_wb = Pel_h[tw1b:tw2b] # Winter 'b' PV Leistung
    Pel_wa = Pel_h[tw1a:tw2a] # Winter 'b' PV Leistung
    Pel_ua = Pel_h[tu1a:tu2a] # Uebergangszeit 'a' PV Leistung
    Pel_ub = Pel_h[tu1b:tu2b] # Uebergangszeit 'b' PV Leistung
    # PV Leistung, Saisonen kombinieren
    Pel_s = Pel_h[ts1:ts2]    # Sommerzeit PV Leistung
    Pel_w = np.concatenate((Pel_wb,Pel_wa)) # PV Winter 'a' und 'b' kombiniert
    Pel_u = np.concatenate((Pel_ua,Pel_ub)) # PV Uebergangszeit 'a' und 'b' kombiniert
    
    # PV Ertraege Zeitreihen
    if (plotFig==1) & (iv==0):
        plt.figure()
        plt.plot(hd/24, Pel_h[:]) # eingabe hier, fuer Zeit: T15m, Td oder Th, Nennleistung: Pel_15m, Pel_1d oder Pel_h
        plt.xlabel('Tage')
        plt.ylabel('PV erzeugte energie (kWh)')
        plt.title('Datenprofil der Photovoltaikanlage - Jahresansicht')
        plt.show()
        if (saveFig==1):
            plt.savefig('Jahresansicht_PV_TaeglicheWerte.png')
    
    # PV Ertraege Saison #eingabe für Pel_s; Pel_w; Pel_u (ua=Frühling)(ub=Herbst)
    tt1 = np.linspace(0,Pel_w.size/(24/freq),Pel_w.size) #Zeitstempel, gleichzeitig mit Pel_w
    if (plotFig==1) & (iv==0):
        plt.figure()
        plt.plot(tt1,Pel_w) # eingabe hier, Nennleistung: Pel_w, Pel_s oder Pel_u, Pel_ua, Pel_ub, Pel_wa, Pel_wb
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('PV erzeugte energie (kWh)')
        plt.title('Datenprofil der Photovoltaikanlage - Winter')
        plt.show()
        if (saveFig==1):
            plt.savefig('Winter_PV.png')
    
    
    # Verbrauchs Last Statistik
    if (plotFig==1) & (iv==0):
        tt1 = np.linspace(0,Ver_h.size/(24/freq),Ver_h.size)
        plt.figure()
        plt.plot(tt1,Ver_h[0:Nhr]) # eingabe: [0:Nhr] -> [start:ende]; z.B. start = np.where(tf=='2013-10-03'), end = np.where(tf=='2013-10-05')
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Standardlast (kWh)')
        plt.title('Datenprofil der Verbrauch (last) - Jahresansicht')
        # anpassen die xticks auf passende Zeit
        plt.axis('tight')
        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
        ax.set_xticks(custom_ticks)
        ax.set_xticklabels(np.rint(custom_ticks/(4*24)))
        plt.show()
        if (saveFig==1):
            plt.savefig('Jahresansicht_Last.png')
    
    # Verbrauch Last Statistik Saison #eingabe für Ver_w; Ver_u; Ver_s; (ua=Frühling ub=Herbst)
    tt1 = np.linspace(0,Ver_s.size/(24/freq),Ver_s.size)
    if (plotFig==1) & (iv==0):
        plt.figure()
        plt.plot(tt1,Ver_s) # eigabe hier: Ver_w, Ver_u, Ver_s, Ver_ua, Ver_ub, Ver_wb, Ver_wa, auch bei tt1 3 Reihen oben
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Standardlast (kWh)')
        plt.title('Datenprofil der Verbrauch (last) -Sommer')
        plt.show()
        if (saveFig==1):
            plt.savefig('Sommer_Last.png')
    
    #-----------------------------------------------------------------------------------------------------
    
    # Verbrauch mit PV energie    
    Pel_hd = scale*Pel_h[selectDate:selectDate+Nd*int(24/freq)] # 1 Tag im Winter-Januar, selectDate=wo der Pel_hd beginnt
    if (Nd ==3): # fuer 3 tag Profil
        Ver_hd = np.concatenate([samW,sonW,werkW], axis=0)
    elif (Nd==1): # fuer 1 Tag profil
        Ver_hd = samW
    elif (Nd==365): # fuer 1 Jahr Profil
        Ver_hd = Ver_h
    else:
        print("Nd is negative")
        
    # Wir nehmen an dass der Speicher eine Anfangs Zustand von 5000 W 
    #Sp0 = np.array([5])
    # Unterschied zwichen PV ertraege und Verbrauch
    dP = (np.asarray(Pel_hd[:]) - np.ravel(Ver_hd)) # dP/dt slope of the curve 
    data_dP[:,iv] = copy.copy(dP)
    # Unterschied samt Speicher kapazitaet
    nr = dP.size
    nc = Sp0.size
    Sp = np.zeros([nr,nc])
    Ncyc = np.zeros(nc)
    # loop mit mehrere Speicher grosse
    for j in range(0,Sp0.size):
        count = int(0)
        S = np.zeros(nr)
        S[0] = Sp0[j]    # Speicher Anfangswert
        # loop mit Zeitreihe grosse
        for i in range(1,dP.size):
            if (dP[i] <= 0) & (S[i-1] <= 0): # Netz Bezug
                S[i] = 0
            elif (dP[i] <= 0) & (S[i-1] > 0): # dP = P-V -ve, aber Speicher noch nicht leer
                S[i] = S[i-1] + dP[i]
                if (S[i-1]==S[0]) & (i!=1): # loop fuer Ladezyklen
                    count = count+1 # ladezyklen + 1
            elif ((dP[i] > 0) & (S[i-1] <= 0)): # dP +ve, Speicher leer
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] >= 0) & (S[i-1] < S[0]):# dP +ve, Speicher ladet
                S[i] = dP[i] + S[i-1]
            elif (dP[i] > 0) & (S[i-1] >= S[0]): # dP +ve, Speicher Kapazitaet erreicht
                S[i] = S[0]
            else:
                print('something went wrong at i=',int(i))
        S[S<0] = 0 # Speicher werte waehrend Netz Bezug
        S[S>S[0]] = S[0] # Speicher werte waehrend Netz einspeisen
        Sp[:,j] = copy.copy(S) 
        Ncyc[j] = count 
    
    #-------Work on this
    # Batteria behaviour at full and no load condition
    N_esp = np.zeros([nr, nc])
    N_ent = np.zeros([nr, nc])
    Splus = np.zeros([nr, nc])
    Sminus = np.zeros([nr,nc])
    cum_N_esp = np.zeros([nr,nc])
    cum_N_ent = np.zeros([nr,nc])
    for ik in range(0,Sp0.size):
            k_ul = np.ravel(np.where(Sp[:,ik]==Sp0[ik])) # speicher voll index
            k_ll = np.ravel(np.where(Sp[:,ik] == 0)) # speicher leer index
            k_plus = np.ravel(np.where((dP>0) & (Sp[:,ik] < Sp0[ik]))) # speicher ladung index
            k_minus = np.ravel(np.where((dP<=0) & (Sp[:,ik]> 0))) # speicher entladung index
            k_esp = np.ravel(np.where((Sp[:,ik]==Sp0[ik]) & (dP>0))) # netz einspeisen index
            k_ent = np.ravel(np.where((Sp[:,ik]==0) & (dP<0))) # netz entnehmen index
            # Speicher ladung und entladung
            Splus[k_plus, ik] = dP[k_plus] # speicher ladung werte
            Sminus[k_minus, ik] = dP[k_minus] # speicher entladung werte
            # Netz einspeisen und entnehmen
            N_esp[k_esp,ik] = dP[k_esp] # energie ins netz eingespeist
            N_ent[k_ent,ik] = dP[k_ent] # energie von netz entnommen
            # cumulative Netz einspeisen und Netz Bezug werte
            cum_N_esp[:,ik] = np.cumsum(N_esp[:,ik])
            cum_N_ent[:,ik] = np.cumsum(N_ent[:,ik])            

    # Treppe funktion fuer parametern
    cumPel_h = np.cumsum(Pel_h)
    cumVer_h = np.cumsum(Ver_h)

    # Speicher Funktionsweise plot      
    if (plotFig==1):
        fig,ax1 = plt.subplots()
        ax2 = ax1.twinx()
        f1 = ax1.plot(hd/24, Ver_hd,'r-',label="Verbrauch")
        f2 = ax1.plot(hd/24, Pel_hd, 'b-',label="PV")
        f3 = ax2.plot(hd/24, Sp[:,plotSp], 'g-',label="Speicher")
        #plt.plot(hd/24, Sp[1:],'r.', label='og')
#        plt.plot(hd/24,N_esp[:,plotSp],'b.',label='N_esp')
#        plt.plot(hd/24,N_ent[:,plotSp],'g.', label='N_ent')
#        ax1.legend(loc=1)
        ax1.set_xlabel('Zeit (Tage)')
        ax1.set_ylabel('Energie (kW)', color='b')
        ax2.set_ylabel('Speicher (kWh)', color='g')
        plt.title('Energie Stuendliche Mittelung')
        # anpassen die xticks auf passende Zeit
#        ax = plt.gca()
#        xmin, xmax = ax.get_xlim()
#        custom_ticks = np.linspace(xmin, xmax, 13, dtype=int)
#        ax.set_xticks(custom_ticks)
#        ax.set_xticklabels(np.round(custom_ticks/(4*24)))
        # set the legends in one legend property
        l1,leg1 = ax1.get_legend_handles_labels()
        l2,leg2 = ax2.get_legend_handles_labels()
        ax2.legend(l1+l2,leg1+leg2,loc=1)
        if (saveFig==1):
            plt.savefig('Speicher_funktionsweise_3Tage.png')
    
    # Treppen funktion darstellen
    if (plotFig==1):
        plt.figure()
        plt.plot(hd/24,cum_N_esp[:,plotSp],'m.-',label='Netz einspeisen')
        plt.plot(hd/24, abs(cum_N_ent[:,plotSp]),'k.-', label = 'Netz Bezug')
        plt.plot(hd/24, abs(cumPel_h),'g.-', label = 'PV Pel')
        plt.plot(hd/24, abs(cumVer_h),'b.-', label = 'Verbrauch')
        plt.legend(loc=0)
        plt.xlabel('Zeit (Tage)')
        plt.ylabel('Energie (kWh)')
        plt.title('Treppenfunktion Jahresansicht ' + str(PV_kwp[iv]) + ' kWh')
        if (saveFig==1):
            plt.savefig('Speicher_treppenfunktion_Jahresansicht.png')
            
    # Treppen funktion  fuer 3-Tagen       
#    if (plotFig==1):
    plt.figure()
    plt.plot(hd[0:72]/24,cum_N_esp[0:72,0],'m.-',label='Netz einspeisen')
    plt.plot(hd[0:72]/24, abs(cum_N_ent[0:72,0]),'k.-', label = 'Netz Bezug')
    plt.plot(hd[0:72]/24, abs(cumPel_h[0:72]),'g.-', label = 'PV Pel')
    plt.plot(hd[0:72]/24, abs(cumVer_h[0:72]),'b.-', label = 'Verbrauch')
    plt.legend(loc=0)
    plt.xlabel('Zeit (Tage)')
    plt.ylabel('Energie (kWh)')
    plt.title('Speicher_Treppenfunktion_3_Tageansicht ' + str(PV_kwp[iv]) + ' kWh')
    if (saveFig==1):
        plt.savefig('Speicher_treppenfunktion.png')

    #------------------------------------------------------------------------------------------------------------------
    # Initialization
    ev_J = np.zeros([nc]) # eigenverbrauch im Jahr
    evQ = np.zeros([nc]) # eingenverbrauchsquote
    Ag = np.zeros([nc]) # Autarkiegrad
    # Produktion im Jahr
    Pel_J = sum(Pel_h) # summe PV leistung im Jahr
    
    if Batterie==1:
        # Eigen verbrauch = (Direcktverbrauch + Batterieentladung) /(Direcktverbrauch + Batterieladung + Netzeinspeisung)
        # Netzeinspeisung ist eine wichtige optimierungsgrosse
        for ik in range(0,Sp0.size):
            # Directverbrauch
            ev_J =  sum(np.fmin(Pel_h, np.ravel(Ver_h)))
#            evQ[ik] = (sum(Ver_h) + abs(sum(Sminus[:,ik])))/(sum(Ver_h) + sum(Splus[:,ik]) + sum(N_esp[1:,ik]))*100
        # Eigen verbrauch = (Direcktverbrauch + Batterieladung) /(Direcktverbrauch + Batterieladung + Netzeinspeisung)
            evQ[ik] = ((ev_J + abs(sum(Splus[:,ik])))/Pel_J)*100
            # Autarkiegrad = (Direcktverbrauch(summe Verbrauch im ganzen Jahr) + Batterieentladung) /(Direcktverbrauch + Batterieentladung + Netzbezug)
#            Ag[ik] = (sum(Ver_h) + abs(sum(Sminus[:,ik])))/(sum(Ver_h) + abs(sum(Sminus[:,ik])) + abs(sum(N_ent[1:,ik])))*100
            # Autarkiegrad = (Direcktverbrauch(summe Verbrauch im ganzen Jahr) + Batterieentladung) /(Direcktverbrauch + Netzbezug)
#            Ag[ik] = (ev_J + abs(sum(Sminus[:,ik])))/(sum(Ver_h) + abs(sum(N_ent[1:,ik])))*100
            Ag[ik] = ((ev_J + abs(sum(Sminus[:,ik])))/(sum(Ver_h)))*100
    else:
        # Eigenverbrauch rechnen =  Erzeugten Energie - eingespeiste Energie
        # indicien finden wo energie erzeugt wird
        ip  = [i for (i,val) in enumerate(Pel_h) if val!=0]
        # minimum finden 
        for ik in range(0,Sp0.size):
            ev_J[ik] = sum(np.fmin(Pel_h[ip], np.ravel(Ver_h[ip]))) # eigenverbrauchsanteil, minumum von Pel_h und Ver_h 
            # eigenverbrauchsquote im jahr  = eigenverbrauchs summe im Jahr/ summe PV Leistung im Jahr
            evQ[ik] = (ev_J[ik]/Pel_J)*100
            # Autarkiegrad = eigenverbrauchs summe im Jahr/summe Verbrauch im Jahr
            Ag[ik] = (ev_J[ik]/sum(Ver_h))*100 
    # loop parameter, kombinieren wegen PV Nennleistung werte in pvFactor     
    data_evQ[:,iv] = copy.copy(evQ)
    data_Ag[:,iv] = copy.copy(Ag)
    data_N_esp[:,iv] = copy.copy(np.sum(N_esp,axis=0))
    data_N_ent[:,iv] = copy.copy(np.sum(N_ent,axis=0))
    data_Ncyc[:,iv] = copy.copy(Ncyc)
#    data_kosten[:,iv] = kosten_Speicher/(Pel_J-sum(Ver_h)) + kosten_PV[iv]/Pel_J
    data_kosten[:,iv] = kosten_Speicher + kosten_PV[iv]
    

    
# Gesamtkosten = kosten_Speicher + kosten_PV + Netz bezug kosten
#gesamtKosten = data_kosten + kosten_Fix + abs(data_N_ent)*preis_N_ent/100
# Gesamterträge €/ kWh = Einspeisevergütung + Stromkostenersparnis
#gesamtErtraege = abs(data_N_esp)*preis_N_esp/100 + sum(Ver_h)*preis_N_ent/100
# Erloese Netz einspeisenverguetung
#Erloese = abs(data_N_esp)*preis_N_esp/100
# new value dtd. 03.01.2019
Erloese = abs(data_N_esp)*preis_N_esp/100 + sum(Ver_h)*preis_N_ent/100 - kosten_Fix
Erloese_1 =  abs(data_N_esp)*preis_N_esp/100
# Kosten = (Speicherkosten + PV kosten)/Lebensdauer Anlage + Netz bezug kosten 
#Kosten = (np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))/Lebensdauer_Anlage + abs(data_N_ent)*preis_N_ent/100
InvestitionsKosten = (np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))
Kosten = ((np.tile(kosten_Speicher,(PV_kwp.size,1)).T + np.tile(kosten_PV,(nc,1)))/Lebensdauer_Anlage) + abs(data_N_ent)*preis_N_ent/100
# Gewinn = Erloese - gesamtKosten
Gewinn = Erloese_1 - Kosten
# Amortisation
#Amortisation = gesamtKosten/(gesamtErtraege - kosten_Fix/Lebensdauer_Anlage)
#Amortisation = (gesamtKosten/(gesamtErtraege - kosten_Fix))/Lebensdauer_Anlage
# new value dtd. 03.01.2019
Amortisation = InvestitionsKosten/Erloese

# eigenverbrauchsquote vs Batterigrosse
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_evQ)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Eigenverbrauchsquote (%)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Eigenverbrauchsquote_BatterieGr.png')

# Autarkiegrad vs Batteriegrosse
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_Ag)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Autarkiegrad (%)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Autarkiegrad_BatterieGr.png')

# Energie aus dem Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_ent))
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Netz Bezug (kWh/a)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzBezug_BatterieGr.png')

# Einspeisestrom
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_N_esp)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Netz Einspeisen (kWh/a)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzEinspeisen_BatterieGr.png')

# Ladezyklen
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,data_Ncyc)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Ladezyklen (-)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Ladezyklen.png')

# Kosten fuer Energie aus dem Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_ent)*preis_N_ent/100)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Netz Bezug kosten (€)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzBezug_preis.png')

# Einspeiseverguetung fuer Energie ins Netz
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0,abs(data_N_esp)*preis_N_esp/100)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Netz Einspeisevergütung (€)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('NetzEinspeisen_preis.png')

# kosten Gesamtkurve
#if (plotFig==1):
#    plt.figure()
#    plt.semilogx(Sp0,gesamtKosten)
#    plt.grid(True)
#    plt.xlabel('Batteriegrosse (kWh)')
#    plt.ylabel('Gesamt kosten (Eur/kWh/a)')
#    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
#    if (saveFig==1):
#        plt.savefig('Kosten_gesamtkurve.png')

# Ertraege Gesamtkurve
#if (plotFig==1):
#    plt.figure()
#    plt.semilogx(Sp0,gesamtErtraege)
#    plt.grid(True)
#    plt.xlabel('Batteriegrosse (kWh)')
#    plt.ylabel('Gesamt Ertraege (Eur/kWh/a)')
#    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
#    if (saveFig==1):
#        plt.savefig('Ertraege_gesamtkurve.png')

# Amortisation
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0, Amortisation)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Amortisation Jahre (Jahre)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Amortisation.png')

# Gewinn
if (plotFig==1):
    plt.figure()
    plt.semilogx(Sp0, Gewinn)
    plt.grid(True)
    plt.xlabel('Batteriegrosse (kWh)')
    plt.ylabel('Gewinn (Eur)')
    plt.legend(['5 kWp', '10 kWp', '15 kWp'], loc=0)
    if (saveFig==1):
        plt.savefig('Gewinn.png')
