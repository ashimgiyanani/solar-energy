# -*- coding: utf-8 -*-
"""
@file: %(file)
@description: 
@syntax:
@inputs:

@outputs:
@example:

See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

@Author: %(username), PhD candidate, TU Delft
Wind energy department, TU Delft
email address: ashimgiyanani@yahoo.com
March 2018; Last revision: 06-04-2018
Created on %(date)s
@version: 0.1
"""

import sys
import math
import numpy as np
import scipy as sp
import pandas as pd
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

from dateutil import parser, rrule
from datetime import datetime, time, date, timedelta
import time
import copy

import glob
import pathlib
import csv
# iteration_utilities import deepflatten
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Eingabe parameter
# peak power in [kWp]
Pp = 15
# Multiplikations Factor fuer Verbrauch lasten [-]
lastFactor=2 # , best combi  =2
# Multiplication Factor fuer PV leistung [-]
pvFactor = np.array([0.7]) # hier ist einen array von 1 element zum Probieren Zweck
# Aktuelle PV Nennleistung nach Anpassung durch der Multiplikations faktor
PV_kwp = pvFactor*Pp
# Speicher Kapazitaet Eingabe in [kWh]
Sp0_1 = np.arange(0,1,0.1) # Speicher kapazitaet Eingabe in kWh
Sp0_2 = np.arange(1,10,1)
Sp0_3 = np.arange(10,60, 10)
Sp0 = np.concatenate([Sp0_1, Sp0_2, Sp0_3], axis=0) # hier ist einen array von 3 arrays kombiniert
#Sp0 = np.array([5]) # hier ist einen array von 1 element zum probieren Zweck
plotSpeicher = 19 # benutzt Sp0[plotSpeicher] = Sp0[3] zu plotten
# parameter wenn die Zeitreihen Interval aendert [Stunden]
freq = 0.25 # Frequenz in Stunden, 1 Stunde = 1, 15 min = 1/4 Stunden
scale = 1.0 # scale factor for increasing the Pel to optimize the pvFactor
# Simulationsparameter fuer die Anzahl der Tage fuer plots und rechnungen. Fuer Kosten simulation nur den 365 Tage nutzen [Tage]
Nd = 365 # no. of days, '1' day for time series related, '3' day for speicher funcktionsweise und '365' fuer Eigenverbrauch und Autarkiesgrad zu rechnen
preis_N_esp = 0.11 # 11c/kWh
preis_N_ent = 0.29 # 29c/kWh
kosten_PV = 1265*PV_kwp # [Eur/kWp]
Lebensdauer_Anlage = 20 # no.of years [Jahre]
Lebensdauer_batt = 15 # no. of years [Jahre]
Nbatt = Lebensdauer_Anlage/Lebensdauer_batt # Anzahl Batterien [-]
kosten_Speicher = 800*Sp0*(Nbatt-1) # Speicher Kosten [Eur/kWh/a]
kosten_Fix = 150 # PV Fix Kosten [Eur]
Batterie = 1 # '1' fuer Batterie basierte rechnungen, '0' - fuer ohne batterie installation
saveFig = 0 # 0 - Grafik Bilder nicht speichern,  '1' - speichern
plotFig = 1 # 0 - Grafik Bilder nicht plotten,  '1' - plotten

# read load profile
file1 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\load_profile1.csv"
data1 = pd.read_csv(file1, sep=',',names=['time','sam_w', 'son_w', 'werk_w','sam_s', 'son_s', 'werk_s','sam_u', 'son_u', 'werk_u' ], header=4)

# read annual PV profile
file2 = r"c:\Users\agiyanani\Downloads\SolarSpeicher\PV_profile_edited.csv"
data2 = pd.read_csv(file2, sep=',',names=['time','Pel_MW','Pel_W'], usecols=[0,1,2], parse_dates=[0], header=0)

# Zeitstempel mit 15-min  Interval
tf = pd.to_datetime(np.arange('2013-01-01', '2014-01-01', np.timedelta64(1,'D'), dtype='datetime64'))

# Taegliche mittelwerte
Td = np.linspace(1,365,365) # 1-day timestamp
Pel_1d  = np.zeros(Td.size)
for i in range(0,Td.size):
    ri = i*24 # Start indicien
    Pel_1d[i] = np.nansum(data2.Pel_W[np.arange(ri, ri+24)]) # Summe von Stuendliche Werte um eine Taegliche Mittelwert zu bauen

# Jahres profil mit Stundliche daten
Pel_h = pvFactor*Pel_1d/1000 # Pel in kW

# indicien fuer die Saisonen
# Winter 'a' - Jan-Maerz Beginn(1) und End(2) indicien
tw1a = int(np.ravel(np.where(tf=='01-01-2013')))
tw2a = int(np.ravel(np.where(tf=='20-03-2013')))
# Uebergangszeit 'a' - Fruehling - Maerz-Mai, Beginn(1) und End(2) indicien
tu1a = int(np.ravel(np.where(tf=='21-03-2013')))
tu2a = int(np.ravel(np.where(tf=='14-05-2013')))
# Sommerzeit, Beginn(1) und End(2) Zeit
ts1 = int(np.ravel(np.where(tf=='15-05-2013')))
ts2 = int(np.ravel(np.where(tf=='14-09-2013')))
# Uebergangszeit 'b' - Herbst - Sept-Okt, Beginn(1) und End(2) indicien
tu1b = int(np.ravel(np.where(tf=='15-09-2013')))
tu2b = int(np.ravel(np.where(tf=='31-10-2013')))
# Winter 'b' - Nov-Dez Beginn(1) und End(2) indicien
tw1b = int(np.ravel(np.where(tf=='11-01-2013')))
tw2b = int(np.ravel(np.where(tf=='31-12-2013')))

# PV Leistung daten in Saisonen
# PV leistung waehrend der Saisonen
Pel_wb = Pel_h[tw1b:tw2b] # Winter 'b' PV Leistung
Pel_wa = Pel_h[tw1a:tw2a] # Winter 'a' PVLeistung
Pel_ua = Pel_h[tu1a:tu2a] # Uebergangszeit 'a' PV Leistung
Pel_ub = Pel_h[tu1b:tu2b] # Uebergangszeit 'b' PV Leistung
# PV Leistung, Saisonen kombinieren
Pel_s = Pel_h[ts1:ts2] # Sommerzeit PV Leistung
Pel_w = np.concatenate((Pel_wb,Pel_wa)) # PV Winter 'a' und 'b' kombiniert
Pel_u = np.concatenate((Pel_ua,Pel_ub)) # PV Uebergangszeit 'a' und 'b' kombiniert

# PV Ertraege
if (plotFig==1):
    plt.figure()
    plt.plot(Td,Pel_h[:]) # eingabe hier
    plt.xlabel('Tage')
    plt.ylabel('PV erzeugte energie (kW)')
    plt.title('Datenprofil der Photovoltaikanlage - Jahresansicht')
    plt.show()
    if (saveFig==1):
        plt.savefig('Jahresansicht_PV_TaeglicheWerte.png')

# PV Ertraege Saison
tt1 = np.linspace(1,Pel_ub.size,Pel_ub.size)
if (plotFig==1):
    plt.figure()
    plt.plot(tt1,Pel_ub) # eingabe hier
    plt.xlabel('Zeit (Tage)')
    plt.ylabel('PV erzeugte energie (kW)')
    plt.title('Datenprofil der Photovoltaikanlage - Herbst')
    plt.show()
    if (plotFig==1):
        plt.savefig('Herbst_PV_Tag.png')

# Verbrauch von 15min Werte zu Stuendliche Werte rechnen
# loop durch Winter daten
samW = lastFactor*np.nansum(data1.sam_w)/1000 # in kW
sonW = lastFactor*np.nansum(data1.son_w)/1000 # in kW
werkW = lastFactor*np.nansum(data1.werk_w)/1000 # in kW
# loop durch Somer daten
samS = lastFactor*np.nansum(data1.sam_s)/1000 # in kW
sonS = lastFactor*np.nansum(data1.son_s)/1000 # in kW
werkS = lastFactor*np.nansum(data1.werk_s)/1000 # in kW
# loop durch Uebergangszeit daten
samU = lastFactor*np.nansum(data1.sam_u)/1000 # in kW
sonU = lastFactor*np.nansum(data1.son_u)/1000 # in kW
werkU = lastFactor*np.nansum(data1.werk_u)/1000 # in kW

# haushalt Verbrauch Daten in Wochen,Saisonen
# Wiederhole die Matrix in der form [5 x 1], um 5 WerkTage zu schaffen
werkW_mat = np.tile(werkW,(5,1))
werkS_mat = np.tile(werkS,(5,1))
werkU_mat = np.tile(werkU,(5,1))
# Matrix kombinieren in einer Woche
week_w = np.vstack((werkW_mat,samW,sonW))
week_s = np.vstack((werkS_mat,samS,sonS))
week_u = np.vstack((werkU_mat,samU,sonU))
# Wiederhole die Matrix zwischen Beginn und End Zeiten in der form [(tw2a-tw1a)/(168/freq) x 1] {PS: 168 = 24 Stunden* 7 Tage der Woche, freq - Stuendlich(1) oder 15 min (0.25)}
Ver_wa = np.tile(week_w,(int(np.floor((tw2a-tw1a)/7)),1)) # winter 'a' Jan-Maerz
Ver_ua = np.tile(week_u,(int(np.ceil((tu2a-tu1a)/7)),1)) # Uebergangszeit 'a' Fruehling - Maerz-Mai
Ver_s = np.tile(week_s,(int(np.floor((ts2-ts1)/7)),1)) # sommerzeit Mai-Sept
Ver_ub = np.tile(week_u,(int(np.ceil((tu2b-tu1b)/7)),1)) # Uebergangszeit 'b' Herbst - Sept-Okt
Ver_wb = np.tile(week_w,(int(np.ceil((tw2b-tw1b)/7)),1)) # Winter 'b' Nov-Dez

Ver_h = np.vstack((Ver_wa,Ver_ua,Ver_s,Ver_ub,Ver_wb,samW)).flatten() # note the adjustment at the end of the year, # ganzen Jahr {PS:wegen der Rundung in 'int(np.floor((tw2a-tw1a)/(168/freq)))' fehlt einen Tag, habe ergaenzt mithilfe von samW}
Ver_w = np.vstack((Ver_wb,samW,Ver_wa)).flatten() # Winter 'a' und Winter 'b' kombiniert, # adjustment as the no. of days didn't match 365 days of the year
Ver_u = np.vstack((Ver_ua,Ver_ub)).flatten() # Uebergangszeit 'a' und 'b' kombiniert
Ver_s = Ver_s.flatten() # die Matrix flach machen

# Verbrauchs Last Statistik
if (plotFig==1):
    plt.figure()
    plt.plot(Td,Ver_h[:]) # eingabe 
    plt.xlabel('Zeit (Tage)')
    plt.ylabel('Standardlast (kW)')
    plt.title('Datenprofil der Verbrauch (last) - Jahresansicht')
    plt.show()
    if (saveFig==1):
        plt.savefig('Jahresansicht_Last_Tag.png')

# Verbrauch Last Statistik Saison
tt1 = np.linspace(1,Ver_ub.size,Ver_ub.size)
if (plotFig==1):
    plt.figure()
    plt.plot(tt1,Ver_ub)
    plt.xlabel('Zeit (Tage)')
    plt.ylabel('Standardlast (kW)')
    plt.title('Datenprofil der Verbrauch (last) - Herbst')
    plt.show()
    if (saveFig==1):
        plt.savefig('Herbst_Last_Tag.png')
